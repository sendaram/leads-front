import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useDispatch } from 'react-redux'
import { LeadsApi } from '../api/LeadsApi'
import AppDataTable from '../components/AppDataTable'
import { CImage } from '@coreui/react'

import imgGreenContacts from '../assets/images/green-contacts.png'
import imgOrangeContacts from '../assets/images/orange-contacts.png'
import imgRedContacts from '../assets/images/red-contacts.png'
import { meCall } from '../api/endpoints'

const Panel = () => {

  const { t } = useTranslation()
  const dispatch = useDispatch();

  const [ leadsList, setLeadsList ] = useState([]);
  const [ loading, setLoading ] = useState(true);
  const [ reloadLeads, setReloadLeads ] = useState(true)

  useEffect(() => {
    if(reloadLeads) {
      (async () => {
        try {
          const data = await LeadsApi.getAllLeads()
          setLeadsList(data);
          setReloadLeads(false)
          setLoading(false);
        } catch (error) {
          dispatch({ type: 'set', errorMessage: error });
          dispatch({ type: 'set', showError: true });
        }
      })()
    }
  }, [reloadLeads])

  useEffect(() => {
    meCall();
  }, [])

  return(
    <>
      <h1>{t('panel.filters')}</h1>
      
      <AppDataTable setData={setLeadsList} data={leadsList} loading={loading} setReload={setReloadLeads}/>
      
      <div className="mt-4 ms-5 contactsLegend">
        <h6>{t('panel.ncontacts')}</h6>
        <ul className="ps-0">
          <li><CImage src={imgGreenContacts} className="align-text-top" /> 1-2</li>
          <li><CImage src={imgOrangeContacts} className="align-text-top" /> 3</li>
          <li><CImage src={imgRedContacts} className="align-text-top" /> 4</li>
        </ul>
      </div>
    </>
  )
}

export default Panel