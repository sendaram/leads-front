import { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useDispatch } from 'react-redux';
import { PlanApi } from '../api/PlanApi';
import { CButton,
  CCol, 
  CContainer, 
  CForm, 
  CFormInput, 
  CRow, CSpinner } from '@coreui/react'
import { LeadsApi } from '../api/LeadsApi';
import { getSystem, updateSystem } from '../api/endpoints';

const Admin = () => {

  const { t } = useTranslation()
  const dispatch = useDispatch()

  const [plans, setPlans] = useState([])
  const [maxLeadContacts, setMaxLeadContacts] = useState(0)
  const [formValues, setFormValues] = useState(null)
  const [formVarValues, setFormVarValues] = useState({
    minCallTime: -1, 
    phoneNumber: -1, 
    maxContacts: -1,
    notificationsEmail: -1,
    maxDays: -1
  })
  const [loadingData, setLoadingData] = useState(true);
  const [formChanged, setFormChanged] = useState(false)
  const [formVarChanged, setFormVarChanged] = useState(false)
  const [resetFields, setResetFields] = useState(true)
  const [timeOut, setTimeOut] = useState(null)
  const [varTimeOut, setVarTimeOut] = useState(null)

  const resetFormValuesFromPlans = (plansList) => {
    let values = {};
    plansList.forEach(plan => {
      values[plan.name] = {
        id: plan.id,
        leadsLimit: plan.leadsLimit
      };
    });
    values["contacts"] = maxLeadContacts;
    setFormValues(values);
  }

  useEffect(() => {
    let isMounted = true;
    (async () => {
      try {
        if( resetFields ) {
          const plansApi = await PlanApi.getAllPlans();
          const systemVars = await getSystem();
          //TODO Get maxLeadContacts
          if (isMounted) {
            setFormVarValues({
              minCallTime: systemVars.minCallTime,
              phoneNumber: systemVars.phoneNumber,
              maxContacts: systemVars.maxContacts,
              maxDays: systemVars.maxDays,
              notificationsEmail: systemVars.notificationsEmail
            });
            setPlans(plansApi)
            setMaxLeadContacts(10)
            resetFormValuesFromPlans(plansApi);
            setLoadingData(false)
          }
          setResetFields(false)
        }
      } catch (error) {
        dispatch({ type: 'set', errorMessage: error });
        dispatch({ type: 'set', showError: true });
      }
    })()
    return () => { isMounted = false };
  }, [resetFields])

  const updatePlan = async (id, value) => {
    try {
      await PlanApi.changePlanLeadsLimit(id, value);
      setFormChanged(false);
    } catch (error) {
      dispatch({ type: 'set', errorMessage: error });
      dispatch({ type: 'set', showError: true });
      setFormChanged(false);
    }
  }

  const updateSystemVars = async (name, value) => {
    try {
      await updateSystem(name, value);
      setFormChanged(false);
    } catch (error) {
      dispatch({ type: 'set', errorMessage: error });
      dispatch({ type: 'set', showError: true });
      setFormChanged(false);
    }
  }

  const onChangeInput = (name, value, id) => {
    setFormValues({...formValues, [name]: {
      id: id,
      leadsLimit: value
    }});

    if(timeOut !== null) {
      clearTimeout(timeOut);
      setTimeOut(null)
    }

    setTimeOut(setTimeout(() => {
        setFormChanged(true);
        updatePlan(id, value);
      }, 1500)
    )
    
  }

  const onChangeVarInput = (name, value) => {
    setFormVarValues({...formValues, [name]: value});
    
    if(varTimeOut !== null) {
      clearTimeout(varTimeOut);
      setVarTimeOut(null)
    }

    setVarTimeOut(setTimeout(() => {
        setFormChanged(true);
        setFormVarChanged(true);
        updateSystemVars(name, value);
      }, 1500)
    )
    
  }

  const importLeads = async () => {
    try {
      await LeadsApi.importLeads();
      dispatch({ type: 'set', infoMessage: 'leads importados' });
      dispatch({ type: 'set', showMessage: true });
    } catch (error) {
      dispatch({ type: 'set', errorMessage: error });
      dispatch({ type: 'set', showError: true });
    }
    
  }

  const resetDatabase = async () => {
    try {
      await LeadsApi.resetDatabase();
      dispatch({ type: 'set', infoMessage: 'database reset' });
      dispatch({ type: 'set', showMessage: true });
    } catch (error) {
      dispatch({ type: 'set', errorMessage: error });
      dispatch({ type: 'set', showError: true });
    }
    
  }

  const importMarcas = async () => {
    try {
      await LeadsApi.importMarcas();
      dispatch({ type: 'set', infoMessage: 'marcas importadas' });
      dispatch({ type: 'set', showMessage: true });
    } catch (error) {
      dispatch({ type: 'set', errorMessage: error });
      dispatch({ type: 'set', showError: true });
    }
    
  }


  const handleForm = (event) => {
    event.preventDefault()
    setFormChanged(false)
  }

  return (
    <>
    {loadingData ?
      <div className="d-flex justify-content-center">
        <CSpinner component="span" aria-hidden="true" color="warning" />
      </div>
    :
    <CContainer>
      <h1>{t('admin.administrator')}</h1>
      <CCol className="text-start">
        <CButton onClick={importLeads} color="primary" className="ms-2">
          {'Importar nuevos leads'}
        </CButton>

        <CButton onClick={importMarcas} color="secondary" className="ms-2">
          {'Importar marcas'}
        </CButton>

        <CButton onClick={resetDatabase} color="primary" className="ms-2">
          {'Reset Database'}
        </CButton>
      </CCol>
      <CCol sm={6}>
        {
          formValues ?
        <CForm onSubmit={handleForm}>
          <h2>{'Configuración de los planes'}</h2>
          {plans.map((plan, index) => (
            <CRow key={index} className="bgwhite mb-3 pt-1 pb-1 d-flex align-items-center">
              <CCol sm={1}>{index+1}</CCol>
              <CCol sm={9}>{t('admin.plan-contactable-leads', { plan: plan.name })}</CCol>
              <CCol sm={2}>
                <CFormInput type="number" id={plan.id} name={plan.name} value={formValues[plan.name].leadsLimit}
                  onChange={e => onChangeInput(e.target.name, e.target.value, e.target.id)} >
                </CFormInput>
              </CCol>
            </CRow>
            ))
          }
          {/*
          <CRow className="bgwhite mb-3 pt-1 pb-1 d-flex align-items-center">
            
            <CCol sm={1}>4</CCol>
            <CCol sm={9}>{t('admin.max-contacts')}</CCol>
            
            <CCol sm={2}>
              <CFormInput type="number" name="contacts" value={formValues["contacts"]}
                onChange={e => onChangeInput(e.target.name, e.target.value)} >
              </CFormInput>
            </CCol>
            
          </CRow>
          */}
        </CForm>
        : ""
      }
      {
        formVarValues.minCallTime !== -1 && formVarValues.phoneNumber !== -1 && formVarValues.maxContacts !== -1 ?
        <CForm onSubmit={handleForm}>
          <h2>{t('admin.system-vars')}</h2>
          <CRow className="bgwhite mb-3 pt-1 pb-1 d-flex align-items-center">
            <CCol sm={1}>1</CCol>
            <CCol sm={9}>{t('admin.min-call')}</CCol>
            <CCol sm={2}>
              <CFormInput 
                disabled={formChanged}
                type="number" id="minCallTime" name="minCallTime" 
                value={formVarValues.minCallTime}
                onChange={e => onChangeVarInput("minCallTime", e.target.value)} >
              </CFormInput>
            </CCol>
          </CRow>
          <CRow className="bgwhite mb-3 pt-1 pb-1 d-flex align-items-center">
            <CCol sm={1}>2</CCol>
            <CCol sm={8}>{t('admin.leads-phone')}</CCol>
            <CCol sm={3}>
              <CFormInput 
                disabled={formChanged}
                type="number" id="phoneNumber" name="phoneNumber" 
                value={formVarValues.phoneNumber}
                onChange={e => onChangeVarInput("phoneNumber", e.target.value)} >
              </CFormInput>
            </CCol>
          </CRow>
          <CRow className="bgwhite mb-3 pt-1 pb-1 d-flex align-items-center">
            <CCol sm={1}>3</CCol>
            <CCol sm={9}>{t('admin.max-contacts')}</CCol>
            <CCol sm={2}>
              <CFormInput 
                disabled={formChanged}
                type="number" id="maxContacts" name="maxContacts" 
                value={formVarValues.maxContacts}
                onChange={e => onChangeVarInput("maxContacts", e.target.value)} >
              </CFormInput>
            </CCol>
          </CRow>
          <CRow className="bgwhite mb-3 pt-1 pb-1 d-flex align-items-center">
            <CCol sm={1}>4</CCol>
            <CCol sm={5}>{t('admin.notifications-email')}</CCol>
            <CCol sm={6}>
              <CFormInput 
                disabled={formChanged}
                type="email" id="notificationsEmail" name="notificationsEmail" 
                value={formVarValues.notificationsEmail}
                onChange={e => onChangeVarInput("notificationsEmail", e.target.value)} >
              </CFormInput>
            </CCol>
          </CRow>
          <CRow className="bgwhite mb-3 pt-1 pb-1 d-flex align-items-center">
            <CCol sm={1}>5</CCol>
            <CCol sm={9}>{t('admin.max-days')}</CCol>
            <CCol sm={2}>
              <CFormInput 
                disabled={formChanged}
                type="number" id="maxDays" name="maxDays" 
                value={formVarValues.maxDays}
                onChange={e => onChangeVarInput("maxDays", e.target.value)} >
              </CFormInput>
            </CCol>
          </CRow>
        </CForm>
        : ""
      }
      </CCol>
    </CContainer>
    }
    </>
  )
}

export default Admin