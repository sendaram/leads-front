import { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { useDispatch, useSelector } from 'react-redux';
import { CCol,
    CImage,
    CRow,
    CSpinner,
} from "@coreui/react"
import CIcon from '@coreui/icons-react'
import { cilArrowLeft } from '@coreui/icons'

import { LeadsApi } from "../api/LeadsApi"
import { DateUtils } from "../utils/DateUtils"
import favoriteYes from '../assets/images/favorite-yes.png'
import contactBook from '../assets/images/contacts.png'
import contactedImg from '../assets/images/contacted.png'
import visualizedImg from '../assets/images/visualized.png'
import ActionsDatatable from '../components/ActionsDatatable';
import { AdminApi } from '../api/AdminApi';


const LeadCardAdmin = () => {

    const { t } = useTranslation()
    let { leadId } = useParams()
    const navigate = useNavigate()
    const dispatch = useDispatch();
    const [lead, setLead] = useState(null);
    const [date, setDate] = useState(null);
    const [loading, setLoading] = useState(false);
    const [leadActions, setLeadActions] = useState([]);

    useEffect(() => {
        let isMounted = true;
        (async () => {
          try {
              if(!lead) {
                const leadApi = await LeadsApi.getLead(leadId);
                const date = DateUtils.getDate(leadApi.fechaAlta);
                const leadActions = await AdminApi.getAdminLeadActions(leadId);
                
                if (isMounted) {
                    setLead(leadApi);
                    setDate(date);
                    setLeadActions(leadActions);
                }
              }
          } catch (error) {
            dispatch({ type: 'set', errorMessage: error });
            dispatch({ type: 'set', showError: true });
          }
        })()
        return () => { isMounted = false };
      }, [])
    

    return(
    <>
    {
    loading ?
        <div className="d-flex justify-content-center">
          <CSpinner component="span" aria-hidden="true" color="warning" />
        </div>
      :
    lead && date ?
        <><CRow>
            <CCol className="me-xl-3 me-xxl-5">
                <CRow className="mb-2">
                    <p className="isLink goBackFromCard" onClick={() => navigate(-1)}><CIcon icon={cilArrowLeft} />
                        {t('common.back-to-list')}
                    </p>
                </CRow>
                <CRow className="ms-2 mb-5">
                    <h4>{t('lead-card.card')}</h4>
                    <CRow className="mt-3 ps-4 pe-xl-2 pe-xxl-5 bgwhite">
                        <CCol className="mb-3">
                            <CRow className="mt-3 align-items-center">
                                <CCol lg={3} xl={2}><CImage src={contactBook} /></CCol>
                                <CCol lg={5} xl={6} className="text-uppercase"><b>{lead.name}</b><br/>{lead.surname}</CCol>
                                <CCol lg={4} xl={4} className="text-center">
                                    <div className="borderblack">{date.day}</div>
                                    <div className="text-uppercase bgorange">{date.month}</div>
                                    <div className="bggrey">{date.year}</div>
                                </CCol>
                            </CRow>
                            <hr />
                            <CRow className="text-center">
                                <CCol xs={3} className="leadCardInfo begrey">{lead.brand}</CCol>
                                <CCol className="leadCardInfo begrey">{lead.model}</CCol>
                                <CCol xs={4}>
                                        <img src={lead.image} alt="" id="leadCardImage" />
                                </CCol>
                            </CRow>
                            <hr />
                            <CRow xs={{ gutterX: 1 }} xl={{ gutterX: 1 }} xxl={{ gutterX: 5 }}>
                                <CCol className="text-center">
                                    <div className="text-uppercase bgorange">{t('lead-card.segment')}</div>
                                    <div className="borderorange">{lead.type}</div>
                                </CCol>
                                <CCol className="text-center">
                                    <div className="text-uppercase bgblue">{t('lead-card.city')}</div>
                                    <div className="borderblue">{lead.province}</div>
                                </CCol>
                                <CCol className="text-center">
                                    <div className="text-uppercase bggrey">{t('lead-card.cp')}</div>
                                    <div className="bordergrey">{lead.cp}</div>
                                </CCol>
                            </CRow>
                            <CRow className="mt-3">
                                {lead.visualizations>0 ? <CCol xs={1}><CImage src={visualizedImg} sizes='sm' /></CCol> : ''}
                                {lead.contactsNumber>0 ? <CCol xs={1}><CImage src={contactedImg} sizes='sm' /></CCol> : ''}
                                {lead.favoritesNumber>0 ? <CCol xs={1}><CImage src={favoriteYes} sizes='sm' /></CCol> : ''}
                            </CRow>
                        </CCol>
                    </CRow>
                </CRow>
                <CRow>
                    <h4>{t('lead-card.lead-statistics')}</h4>
                    <div className="bgdarkgrey mt-3 pb-4 ps-4">
                        <CRow className="ps-0 pb-2 pt-3 me-4 bbwhite">
                            <CCol className="text-uppercase"><CImage className="align-text-top" src={visualizedImg} sizes='sm' />&nbsp;{t('lead-card.visualized')}</CCol>
                            <CCol className="text-end">{lead.visualizations}</CCol>
                        </CRow>
                        <CRow className="ps-0 pb-2 pt-3 me-4 bbwhite">
                            <CCol className="text-uppercase"><CImage className="align-text-top" src={favoriteYes} sizes='sm' />&nbsp;{t('lead-card.favorite')}</CCol>
                            <CCol className="text-end">{lead.favoritesNumber}</CCol>
                        </CRow>
                        <CRow className="ps-0 pb-2 pt-3 me-4 bbwhite">
                            <CCol className="text-uppercase"><CImage className="align-text-top" src={contactedImg} sizes='sm' />&nbsp;{t('lead-card.contacted')}</CCol>
                            <CCol className="text-end">{lead.contactsNumber}</CCol>
                        </CRow>
                    </div>
                </CRow>
            </CCol>
            <CCol className="ms-xl-3 ms-xxl-5">
                <CRow className="mb-3">
                <h4>Acciones</h4>
                    <ActionsDatatable setData={null} data={leadActions} loading={false} setReload={null}/>
                </CRow>
            </CCol>
        </CRow>
        </>
    : ''
    }
    </>
    )
}

export default LeadCardAdmin