import { useEffect, useState } from 'react';
import { useNavigate, useParams, useSearchParams } from 'react-router-dom';
import { CContainer } from '@coreui/react';
import { useTranslation } from 'react-i18next';
import { UserApi } from '../api/UserApi';

const ConfirmEmail = (props: any) => {

	const navigate = useNavigate();
	
	const { t } = useTranslation()
	let [searchParams, setSearchParams] = useSearchParams();
	let [messageCode, setMessageCode] = useState('0001');

	//Listening for logging in process, for showing spinner
	useEffect(() => {
		let isMounted = true;
      	(async () => {
			if(searchParams.get("id") !== null){
				if(isMounted) {
					try {
						const response = await UserApi.confirmEmail(searchParams.get("id"));
						setMessageCode(response.code);
						setTimeout(() => {
							navigate("/login");
						}, 2500);
					} catch (error) {
						setMessageCode(error);
						setTimeout(() => {
							navigate("/login");
						}, 2500);
					}
				}
			} else {

			}
		})();
		return () => { isMounted = false };
	}, [])


	return (
		<CContainer sm className="loginContainer">
			<div className="loginWrapper">
				<h1 className="text-center mb-5 loginHeader">
					{t('remote.'+messageCode)}
				</h1>
			</div>
		</CContainer>
	);
}

export default ConfirmEmail;
