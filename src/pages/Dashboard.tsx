import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import {
  CCol,
  CRow,
  CSpinner
} from '@coreui/react'
import { StatisticsApi } from '../api/StatisticsApi'
import { useDispatch } from 'react-redux'

const Dashboard = () => {

  const { t } = useTranslation();
  const dispatch = useDispatch();

  const [loadingData, setLoadingData] = useState(true);
  const [topBrands, setTopBrands] = useState([]);
  const [topSegments, setTopSegments] = useState([]);
  const [general, setGeneral] = useState([]);
  const [prices, setPrices] = useState([{value:0},{value:0},{value:0},{value:0},{value:0},{value:0},{value:0}]);
  const [misLeads, setMisLeads] = useState([]);
  const [provincias, setProvincias] = useState([]);
  
  useEffect(() => {
    let isMounted = true;
    (async () => {
      try {
        const statistics = await StatisticsApi.getDashboard();
        if(isMounted) {
          setTopBrands(statistics.marcas.slice(0, 5));
          setTopSegments(statistics.segmentos);
          setGeneral(statistics.general);
          setMisLeads(statistics.misLeads);
          setProvincias(statistics.provincias);
          setPrices(statistics.precios);
          setLoadingData(false)
        }
      } catch (error) {
        dispatch({ type: 'set', errorMessage: error });
        dispatch({ type: 'set', showError: true });
      }
    })()

    return () => { isMounted = false };
  }, [])

  const topPrices = [
    { label : t('dashboard.top-price-1'), value : prices[0].value },
    { label : t('dashboard.top-price-2'), value : prices[1].value },
    { label : t('dashboard.top-price-3'), value: prices[2].value },
    { label : t('dashboard.top-price-4'), value : prices[3].value },
    { label : t('dashboard.top-price-5'), value: prices[4].value },
  ]

  return (
    <>
      {loadingData ?
        <div className="d-flex justify-content-center">
          <CSpinner component="span" aria-hidden="true" color="warning" />
        </div>
      :
      <CRow className="mb-4">
        <CCol xs={12} sm={6} xl={4} className="pt-3">
          <h2>{t('dashboard.general')}</h2>
          <div className="bgwhite mt-3 pb-4 ps-4">
            { general ? general.map((lead, index) => (
              <CRow key={index} className="ps-0 pb-2 pt-3 me-4 bbdarkgrey">
                  <CCol>{lead.label}</CCol>
                  <CCol className="text-end dashboardValue">{lead.value}</CCol>
              </CRow>
              )) : " "}
          </div>
        </CCol>
        <CCol xs={12} sm={6} xl={4} className="pt-3">
          <h2>{t('dashboard.top-provinces')}</h2>
          <div className="bgwhite mt-3 pb-4 ps-4">
            { provincias ? provincias.map((provincia, index) => (
              <CRow key={index} className="ps-0 pb-2 pt-3 me-4 bbdarkgrey">
                  <CCol>{provincia.label}</CCol>
                  <CCol className="text-end dashboardValue">{provincia.value}</CCol>
              </CRow>
              )) : ""}
          </div>
        </CCol>
        <CCol xs={12} sm={6} xl={4} className="pt-3">
          <h2>{t('dashboard.top-brands')}</h2>
          <div className="bgwhite mt-3 pb-4 ps-4">
            { topBrands ? topBrands.map((brand, index) => (
              <CRow key={index} className="ps-0 pb-2 pt-3 me-4 bbdarkgrey">
                  <CCol xs={1}>
                    {brand.image ? 
                      <img src={brand.image} alt="" style={{width: "25px", height: "auto"}} />
                      : ''
                    }
                  </CCol>
                  <CCol className="ps-4">{brand.label}</CCol>
                  <CCol className="text-end dashboardValue">{brand.value}</CCol>
              </CRow>
              )) : ""}
          </div>
        </CCol>
        <CCol xs={12} sm={6} xl={4} className="pt-3">
          <h2>{t('dashboard.my-leads')}</h2>
          <div className="bgdarkgrey mt-3 pb-4 ps-4">
            { misLeads ? misLeads.map((leads, index) => (
              <CRow key={index} className="ps-0 pb-2 pt-3 me-4 bbwhite">
                  <CCol>{leads.label}</CCol>
                  <CCol className="text-end">{leads.value}</CCol>
              </CRow>
              )) : ""}
          </div>
        </CCol>
        <CCol xs={12} sm={6} xl={4} className="pt-3">
          <h2>{t('dashboard.top-segments')}</h2>
          <div className="bgwhite mt-3 pb-4 ps-4">
            { topSegments ? topSegments.map((segment, index) => (
              <CRow key={index} className="ps-0 pb-2 pt-3 me-4 bbdarkgrey">
                  <CCol>{segment.label}</CCol>
                  <CCol className="text-end dashboardValue">{segment.value}</CCol>
              </CRow>
              )) : ""}
          </div>
        </CCol>
        <CCol xs={12} sm={6} xl={4} className="pt-3">
          <h2>{t('dashboard.top-prices')}</h2>
          <div className="bgwhite mt-3 pb-4 ps-4">
            { topPrices.map((price, index) => (
              <CRow key={index} className="ps-0 pb-2 pt-3 me-4 bbdarkgrey">
                  <CCol sm={10}>{price.label}</CCol>
                  <CCol className="text-end dashboardValue">{price.value}</CCol>
              </CRow>
              ))}
          </div>
        </CCol>
      </CRow>
    }
    </>
  )
}

export default Dashboard
