import React from 'react'
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { CContainer } from '@coreui/react';

const Cancel = () => {
    const navigate = useNavigate();
    const { t } = useTranslation();

    setTimeout(() => {
        navigate("/dashboard");
    }, 3000);

  return (
    <CContainer sm className="responseContainer">
        <div className="responseWrapper">
            <h1 className="text-center mb-5 responseHeader">{t('stripe-response.cancel-title')}</h1>
            <div className="text-center">{t('stripe-response.cancel-redirect')}</div>
            <div className='loader'></div>
        </div>
    </CContainer>
  )
}

export default Cancel