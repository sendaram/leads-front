import { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { useDispatch, useSelector } from 'react-redux';
import { CButton, 
    CCol,
    CForm,
    CFormInput,
    CFormLabel,
    CFormTextarea,
    CImage,
    CModal,
    CModalBody,
    CRow,
    CSpinner,
    CTable,
    CTableBody,
} from "@coreui/react"
import ModalAction from '../components/ModalAction'
import ActionPopover from '../components/ActionPopover'
import { ToastNotify } from '../components/ToastNotify';
import CIcon from '@coreui/icons-react'
import { cilArrowLeft } from '@coreui/icons'

import { LeadsApi } from "../api/LeadsApi"
import { DateUtils } from "../utils/DateUtils"
import { meCall } from '../api/endpoints';
import favoriteYes from '../assets/images/favorite-yes.png'
import favorite from '../assets/images/favorite.png'
import contactBook from '../assets/images/contacts.png'
import contactedImg from '../assets/images/contacted.png'
import visualizedImg from '../assets/images/visualized.png'


const LeadCard = () => {

    const { t } = useTranslation()
    let { leadId } = useParams()
    const navigate = useNavigate()

    const user = useSelector((state: any) => state.auth.user);
    const dispatch = useDispatch();

    const [lead, setLead] = useState(null);
    const [reference, setReference] = useState(null);
    const [phone, setPhone] = useState(null);
    const [date, setDate] = useState(null);
    const [loading, setLoading] = useState(false);
    const [showModalCall, setShowModalCall] = useState(false);
    const [showModalEmail, setShowModalEmail] = useState(false);
    const [showModalAction, setShowModalAction] = useState(false);
    const [showWarningBuyLead, setShowWarningBuyLead] = useState(false);

    const [leadActions, setLeadActions] = useState(null);
    const [selectedAction, setSelectedAction] = useState(null);
    const [globalLeadActions, setGlobalLeadActions] = useState(null);

    const [actionClicked, setActionClicked] = useState(null);
    const [isMyLead, setIsMyLead] =  useState(false);
    const [isFavorite, setIsFavorite] =  useState(false);

    //For modal Email
    const [emailValues, setEmailValues] = useState({subject : "", message : ""})

    useEffect(() => {
        let isMounted = true;
        (async () => {
          try {
              if(!lead) {
                const leadApi = await LeadsApi.getLead(leadId);
                const isMyLeadApi = await LeadsApi.isMyLead(leadId);
                const isFavoriteApi = await LeadsApi.isFavorite(leadId);
                const date = DateUtils.getDate(leadApi.fechaAlta);
                if (isMounted) {
                    setLead(leadApi);
                    setDate(date);
                    setIsMyLead(isMyLeadApi);
                    setIsFavorite(isFavoriteApi);
                }
              }
          } catch (error) {
            dispatch({ type: 'set', errorMessage: error });
            dispatch({ type: 'set', showError: true });
          }
        })()
        return () => { isMounted = false };
      }, [])

      useEffect(() => {
        let isMounted = true;
        (async () => {
          try {
            const leadActions = await LeadsApi.getLeadActions(leadId);
            if (isMounted) {
                setLeadActions(leadActions.actions);
                setGlobalLeadActions(leadActions.globalActions);
            }
          } catch (error) {
            dispatch({ type: 'set', errorMessage: error });
            dispatch({ type: 'set', showError: true });
          }
        })()
        return () => { isMounted = false };
      }, [showModalAction])

    //Cuando se pulsa el botón llamar o email, se setea actionClicked y se ejecuta este useEffect,
    //que comprueba si el usuario ya es un contacto, y si no lo es muestra el warning de descontar lead.
    useEffect(() => {
        if( isMyLead ) {
            doActionCallOrEmail();
        } else if( actionClicked!=null ) {
            setShowWarningBuyLead(true);
        }
    }, [actionClicked])

    const doActionCallOrEmail = () => {
        setShowWarningBuyLead(false)
        if(actionClicked === 'call')
            llamarAction();
        else if(actionClicked === 'email')
            sendEmailAction();
        setActionClicked(null);
    }

    const llamarAction = async () => {
        if( user.plan.name ) {
            try {
                if(!isMyLead) {
                    // Añadimos el lead a mis leads (si no está ya en ellos)
                    try {
                        const result = await LeadsApi.addUserLead(leadId);
                    } catch (error) {
                        dispatch({ type: 'set', errorMessage: error });
                        dispatch({ type: 'set', showError: true });
                        return;
                    }
                    const isMyLeadApi = await LeadsApi.isMyLead(leadId);
                    setIsMyLead(isMyLeadApi)
                } 
                // Creamos un nuevo contacto y lo guardamos
                const contactDto = await LeadsApi.contactLead(leadId);
                const leadActions = await LeadsApi.getLeadActions(leadId);

                const reference = contactDto.reference;
                const phone = contactDto.phone;

                setLeadActions(leadActions.actions);
                setGlobalLeadActions(leadActions.globalActions);
                setReference(reference);
                setPhone(phone);
                setShowModalCall(true);
                await meCall();
            } catch (error) {
                ToastNotify.error(error);
            }
        } else {
            //Show modal for buy plan
            dispatch({ type: 'plans', showPlans: true });
        }
    }

    const sendAction = async (action) => {
        try {
            setLoading(true);
            const response = await LeadsApi.sendAction(
                lead.id,
                action,
                ""
            );
            const leadActions = await LeadsApi.getLeadActions(leadId);
            setLeadActions(leadActions.actions);
            setGlobalLeadActions(leadActions.globalActions);
            
        } catch (error) {
            dispatch({ type: 'set', errorMessage: error });
            dispatch({ type: 'set', showError: true });
        }
        setLoading(false)
    }

    const sendEmailAction = () => {
        if( user.plan.name ) {
            setShowModalEmail(true);
        } else {
            //Show modal for buy plan
            dispatch({ type: 'plans', showPlans: true });
        }
    }

    const ButtonAction = async (action) => {
        if( user.plan.name ) {
            try {
                setSelectedAction(action);
                setShowModalAction(true);
            } catch (error) {
                console.log('ButtonAction Error: ', error)
            }
        } else {
            //Show modal for buy plan
            dispatch({ type: 'plans', showPlans: true });
        }
    }

    const handleSendEmail = async (event) => {
        event.preventDefault();
        const data = new FormData(event.target);
        try {
            if(!isMyLead) {
                await LeadsApi.addUserLead(leadId);
                const isMyLeadApi = await LeadsApi.isMyLead(leadId);
                setIsMyLead(isMyLeadApi)
            }
            const response = await LeadsApi.emailLead(leadId, data.get('subject'), data.get('message'));
            dispatch({ type: 'set', remoteMessage: response.code });
            dispatch({ type: 'set', showRemoteMessage: true });
            const leadActions = await LeadsApi.getLeadActions(leadId);
            setLeadActions(leadActions.actions);
            setGlobalLeadActions(leadActions.globalActions);
            await meCall();
            
        } catch (error) {
            dispatch({ type: 'set', errorMessage: error });
            dispatch({ type: 'set', showError: true });
        }
        setShowModalEmail(false)
    }

    //Methods for handleFavorite button
    const addFavoriteLead = async (id) => {
      try {
        await LeadsApi.addFavorite( id );
        setIsFavorite(!isFavorite);
      } catch (error) {
        dispatch({ type: 'set', errorMessage: error });
        dispatch({ type: 'set', showError: true });
      }
    }

    const removeFavoriteLead = async (id) => {
        try {
          await LeadsApi.removeFavorite( id );
          setIsFavorite(!isFavorite);
        } catch (error) {
          dispatch({ type: 'set', errorMessage: error });
          dispatch({ type: 'set', showError: true });
        }
    }

    return(
    <>
    {
    loading ?
        <div className="d-flex justify-content-center">
          <CSpinner component="span" aria-hidden="true" color="warning" />
        </div>
      :
    lead && date ?
        <><CRow>
            <CCol className="me-xl-3 me-xxl-5">
                <CRow className="mb-2">
                    <p className="isLink goBackFromCard" onClick={() => navigate(-1)}><CIcon icon={cilArrowLeft} />
                        {t('common.back-to-list')}
                    </p>
                </CRow>
                <CRow className="ms-2 mb-5">
                    <h4>{t('lead-card.card')}</h4>
                    <CRow className="mt-3 ps-4 pe-xl-2 pe-xxl-5 bgwhite">
                        <CCol className="mb-3">
                            <CRow className="mt-3 align-items-center">
                                <CCol lg={3} xl={2}><CImage src={contactBook} /></CCol>
                                <CCol lg={5} xl={6} className="text-uppercase"><b>{lead.name}</b><br/>{lead.surname}</CCol>
                                <CCol lg={4} xl={4} className="text-center">
                                    <div className="borderblack">{date.day}</div>
                                    <div className="text-uppercase bgorange">{date.month}</div>
                                    <div className="bggrey">{date.year}</div>
                                </CCol>
                            </CRow>
                            <hr />
                            <CRow className="text-center">
                                <CCol xs={3} className="leadCardInfo begrey">{lead.brand}</CCol>
                                <CCol className="leadCardInfo begrey">{lead.model}</CCol>
                                <CCol xs={4}>
                                        <img src={lead.image} alt="" id="leadCardImage" />
                                </CCol>
                            </CRow>
                            <hr />
                            <CRow xs={{ gutterX: 1 }} xl={{ gutterX: 1 }} xxl={{ gutterX: 5 }}>
                                <CCol className="text-center">
                                    <div className="text-uppercase bgorange">{t('lead-card.segment')}</div>
                                    <div className="borderorange">{lead.type}</div>
                                </CCol>
                                <CCol className="text-center">
                                    <div className="text-uppercase bgblue">{t('lead-card.city')}</div>
                                    <div className="borderblue">{lead.province}</div>
                                </CCol>
                                <CCol className="text-center">
                                    <div className="text-uppercase bggrey">{t('lead-card.cp')}</div>
                                    <div className="bordergrey">{lead.cp}</div>
                                </CCol>
                            </CRow>
                            <CRow className="mt-3">
                                {lead.visualizations>0 ? <CCol xs={1}><CImage src={visualizedImg} sizes='sm' /></CCol> : ''}
                                {lead.contactsNumber>0 ? <CCol xs={1}><CImage src={contactedImg} sizes='sm' /></CCol> : ''}
                                {lead.favoritesNumber>0 ? <CCol xs={1}><CImage src={favoriteYes} sizes='sm' /></CCol> : ''}
                            </CRow>
                        </CCol>
                    </CRow>
                </CRow>
                <CRow>
                    <h4>{t('lead-card.lead-statistics')}</h4>
                    <div className="bgdarkgrey mt-3 pb-4 ps-4">
                        <CRow className="ps-0 pb-2 pt-3 me-4 bbwhite">
                            <CCol className="text-uppercase"><CImage className="align-text-top" src={visualizedImg} sizes='sm' />&nbsp;{t('lead-card.visualized')}</CCol>
                            <CCol className="text-end">{lead.visualizations}</CCol>
                        </CRow>
                        <CRow className="ps-0 pb-2 pt-3 me-4 bbwhite">
                            <CCol className="text-uppercase"><CImage className="align-text-top" src={favoriteYes} sizes='sm' />&nbsp;{t('lead-card.favorite')}</CCol>
                            <CCol className="text-end">{lead.favoritesNumber}</CCol>
                        </CRow>
                        <CRow className="ps-0 pb-2 pt-3 me-4 bbwhite">
                            <CCol className="text-uppercase"><CImage className="align-text-top" src={contactedImg} sizes='sm' />&nbsp;{t('lead-card.contacted')}</CCol>
                            <CCol className="text-end">{lead.contactsNumber}</CCol>
                        </CRow>
                    </div>
                </CRow>
            </CCol>
            <CCol className="ms-xl-3 ms-xxl-5">
                <CRow className="mb-3">
                    <div className="d-md-block">
                    <CButton color="danger" className="me-1 borderorange btnorange" disabled={(user && user.role === 'admin') || lead.vendido || lead.borrado} onClick={ () => setActionClicked('call') }>{t('lead-card.call')}</CButton>
                    <CButton color="info" className="me-1 btnblue" disabled={(user && user.role === 'admin' ) || lead.vendido || lead.borrado} onClick={ () => setActionClicked('email') }>{t('lead-card.send-email')}</CButton>
                    <CButton color="success" onClick={ () =>  ButtonAction("VENDIDO") } disabled={!isMyLead || lead.vendido || lead.borrado}>
                        Vendido
                    </CButton>
                    {/*
                    <CButton color="dark" onClick={ ButtonAction } disabled={!isMyLead || lead.vendido || lead.borrado}>{t('lead-card.action')}</CButton>
                    */}
                    { user && user.role!=='admin' ?
                        isFavorite ?
                        <CButton className='ms-2 leadBtnFavoriteYes' onClick={ () => removeFavoriteLead(leadId) }><CImage src={favorite} /></CButton>
                        : 
                        <CButton className='ms-2 leadBtnFavoriteNo' onClick={ () => addFavoriteLead(leadId) }><CImage src={favorite} /></CButton>
                    :
                    <></>
                    }

                    </div>
                </CRow>
                <CRow className="mb-3">
                    <div className="d-md-block">
                    <CButton className="me-1" color="dark" onClick={() => ButtonAction("PASADO_A_CRM") } disabled={!isMyLead || lead.vendido || lead.borrado}>
                        Contactado / CRM 
                    </CButton>
                    <CButton className="me-1" color="dark" onClick={ () => ButtonAction("NO_INTERESADO") } disabled={!isMyLead || lead.vendido || lead.borrado}>
                        No interesado
                    </CButton>
                    <CButton color="dark" onClick={ () =>  ButtonAction("YA_HA_COMPRADO") } disabled={!isMyLead || lead.vendido || lead.borrado || user.cancelled}>
                        Ya ha comprado
                    </CButton>
                    
                    </div>
                </CRow>
                
                {globalLeadActions && globalLeadActions.length > 0 ?
                <div>
                    <h4>{t('lead-card.global-record')}</h4>

                    <CRow className="mt-3 pb-5 ps-xl-2 pe-xl-2 ps-xxl-5 pe-xxl-5 bgwhite">
                        <CCol>
                            <CTable align="middle" className="mt-3 leadHistoryTable" borderless responsive>
                                <CTableBody>
                                    {
                                    globalLeadActions.map((item, key) => {
                                        if(item.type =='VENDIDO' && !item.confirmed) {
                                            return( <ActionPopover key={key} system={item.system} title={item.type} date={item.createdAt} content={'Este lead ha sido marcado como vendido por otro usuario, pero la venta aún no ha sido confirmada.'} /> )
                                        } else if (item.type =='VENDIDO' && item.confirmed) {
                                            return( <ActionPopover key={key} system={item.system} title={item.type} date={item.createdAt} content={'Este lead ha sido marcado como vendido por otro usuario, y la venta ha sido confirmada.'} /> )
                                        } else {
                                            return( <ActionPopover key={key} system={item.system} title={item.type} date={item.createdAt} content={item.description} /> )
                                        }
                                        
                                    })
                                    }
                                </CTableBody>
                            </CTable>
                        </CCol>
                    </CRow>
                </div>
                :''
                }
                 <h4>{t('lead-card.record')}</h4>
                <CRow className="mt-3 pb-5 ps-xl-2 pe-xl-2 ps-xxl-5 pe-xxl-5 bgwhite">
                    <CCol>
                        {leadActions && leadActions.length>0 ?
                        <CTable align="middle" className="mt-3 leadHistoryTable" borderless responsive>
                            <CTableBody>
                                {leadActions.map((item, key) => {
                                    return( <ActionPopover key={key} system={item.system} title={item.type} date={item.createdAt} content={item.description} /> )
                                })}
                            </CTableBody>
                        </CTable>
                        : 
                        <div className="mt-3">{t('lead-card.empty-records')}</div>
                        }
                    </CCol>
                </CRow>
            </CCol>
        </CRow>
        <CModal alignment="center" visible={showWarningBuyLead} onClose={() => setShowWarningBuyLead(false)}>
            <CModalBody className="text-center">
                <h2>{t('lead-card.warning')}</h2>
                <h5 className="mb-3">{t('lead-card.discount-lead')}</h5>
                <div className="mt-3">
                    <CButton onClick={() => doActionCallOrEmail()}>{t('lead-card.accept-discount')}</CButton>
                    <CButton onClick={() => setShowWarningBuyLead(false)} color="secondary" className="ms-2">{t('lead-card.cancel')}</CButton>
                </div>
            </CModalBody>
        </CModal>
        <CModal alignment="center" visible={showModalCall} onClose={() => setShowModalCall(false)}>
            <CModalBody className="text-center">
                <h5>{t('lead-card.modal-call-phone')}</h5>
                <h2 className="mb-3">{phone}</h2>
                <p>{t('lead-card.modal-call-ref')}<b><span className="bgorange ps-2 pe-2 pt-2 pb-2 ms-2">{reference}</span></b></p>
            </CModalBody>
        </CModal>
        <CModal alignment="center" visible={showModalEmail} onClose={() => setShowModalEmail(false)}>
            <CModalBody>
                <CForm id="cardFormEmail" onSubmit={handleSendEmail}>
                    <CFormLabel htmlFor="formNameTo">{t('lead-card.dear-opening')}</CFormLabel> 
                    <CFormInput type="text" id="formNameTo" name="formNameTo" placeholder={lead.name+" "+lead.surname} readOnly plainText />
                    <CFormLabel htmlFor="subject" className="mt-2">{t('lead-card.subject')}</CFormLabel>
                    <CFormInput type="text" id="subject" name="subject"
                        onChange={e => setEmailValues({...emailValues, [e.target.name]: e.target.value})} ></CFormInput>
                    <CFormLabel htmlFor="formEmail" className="mt-2">{t('lead-card.message')}</CFormLabel>
                    <CFormTextarea id="formEmail" name="message"
                        onChange={e => setEmailValues({...emailValues, [e.target.name]: e.target.value})} ></CFormTextarea>
                    <div className="mt-3">
                        <CButton type="submit">{t('lead-card.send-email')}</CButton>
                        <CButton onClick={() => setShowModalEmail(false)} color="secondary" className="ms-2">{t('lead-card.cancel')}</CButton>
                    </div>
                </CForm>
            </CModalBody>
        </CModal>
        <CModal alignment="center" visible={showModalAction} onClose={() => setShowModalAction(false)}>
            <CModalBody>
                <ModalAction lead={lead} setShow={setShowModalAction} action={selectedAction}/>
            </CModalBody>
        </CModal>
        </>
    : ''
    }
    </>
    )
}

export default LeadCard