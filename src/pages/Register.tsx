import { useEffect, useState } from 'react';
import { NavLink, useNavigate } from 'react-router-dom'
import { isEmpty, size } from 'lodash';
import { ToastContainer } from 'react-toastify';
import { ToastNotify } from '../components/ToastNotify';
import { useTranslation } from 'react-i18next'
import { CButton, 
	CContainer,
	CForm, 
	CFormInput, 
	CFormLabel, 
	CImage, 
	CRow,
	CSpinner } from '@coreui/react';
import { registerCall } from '../api/endpoints';

import logoImg from '../assets/images/spotileads_positivo.png'
import hidePassImg from '../assets/images/hide-pass.png'
import hidePassHoverImg from '../assets/images/hide-pass-hover.png'
import revealPassImg from '../assets/images/reveal-pass.png'
import revealPassHoverImg from '../assets/images/reveal-pass-hover.png'

const Register = (props: any) => {

	const navigate = useNavigate();
	
	const defaultFormValue = () => {
		return {
			email: "",
			password: "",
			name: "",
			companyName: ""
		}
	}
	const { t } = useTranslation()
	
	const [showPass, setShowPass] = useState(false);
	const [formData, setformData] = useState(defaultFormValue());
	const [loading, setLoading] = useState(false);
	const [loadingSpinner, setLoadingSpinner] = useState("");

	//Listening for logging in process, for showing spinner
	useEffect(() => {
		loading
		? setLoadingSpinner(getSpinner)
		: setLoadingSpinner("");
	}, [loading])
	const getSpinner: any = ( <CSpinner component="span" size="sm" aria-hidden="true" color="warning" /> )

	// Envio a la api de los datos para hacer login
	const onSubmit = async () => {
		if ((isEmpty(formData.email) || isEmpty(formData.password) || isEmpty(formData.name) || isEmpty(formData.companyName) ) ) {
			ToastNotify.error(t('login.error-fields-mandatory'));
		} else if (size(formData.password) < 6 ) {
			ToastNotify.error(t('login.error-password-minimum'));
		} else {
			setLoading(true);
			try {
				const registerResponse = await registerCall(
					formData.email, 
					formData.password, 
					formData.name, 
					formData.companyName
				);
					ToastNotify.success(t('remote.'+registerResponse.code));
				setTimeout(() => {
					setLoading(false);
					navigate("/login");
				}, 2500);
			} catch(error) {
				ToastNotify.error(t('remote.'+error));
				setLoading(false);
				return;
			}
			// { access_token: string, refresh_token: string, expires_in: number, token_type: string, scope: any }
			setLoading(false);
			setformData(defaultFormValue());
		}
	}

	// Cada vez que cambian los datos del formulario
	const onChange = (e: any, fieldName: string) => {
		setformData({ ...formData, [fieldName]: e.nativeEvent.target.value });
	}

	//Event for reveal or hide password
	const onClickRevealPass = () => {
		setShowPass(!showPass)
	}

	const getShowPassSrc = (event, mouseOver) => {
		let src = '';
		if (showPass) {
			mouseOver
			? src = hidePassHoverImg
			: src = hidePassImg
		} else {
			mouseOver
			? src = revealPassHoverImg
			: src = revealPassImg
		}
		event.currentTarget.src = src;
	}

	return (
		<CContainer sm className="registerContainer">
			<div className="loginWrapper">
				<h1 className="text-center mb-5 loginHeader">{t('register.header')}</h1>
				<div className="text-center logoWrapper"><CImage src={logoImg} className="mb-3" /></div>
				<CForm className="loginForm mt-2">
					<div className="mb-3">
						<CFormLabel htmlFor="inputEmail">{t('login.email')}</CFormLabel>
						<CFormInput onChange={(e) => onChange(e, "email")} type="email" id="inputEmail" />
					</div>
					<div className="mb-3">
						<CFormLabel htmlFor="inputNAme">{t('register.name')}</CFormLabel>
						<CFormInput onChange={(e) => onChange(e, "name")} type="text" id="inputName" />
					</div>
					<div className="mb-2 d-flex align-items-center">
						<div className="flex-grow-1">
							<CFormLabel htmlFor="inputPassword">{t('login.password')}</CFormLabel>
							<CFormInput onChange={(e) => onChange(e, "password")} type={showPass ? "text" : "password"} id="inputPassword" />
						</div>
						<div>
							{showPass
							? <CImage src={hidePassImg} onMouseOver={e => getShowPassSrc(e, true)} onMouseOut={e => getShowPassSrc(e, false)} 
									onClick={() => onClickRevealPass()} className="pt-4 isLink" />
							: <CImage src={revealPassImg} onMouseOver={e => getShowPassSrc(e, true)} onMouseOut={e => getShowPassSrc(e, false)} 
									onClick={() => onClickRevealPass()} className="pt-4 isLink" />}
						</div>
					</div>
					<div className="mb-3">
						<CFormLabel htmlFor="companyName">{t('register.companyName')}</CFormLabel>
						<CFormInput onChange={(e) => onChange(e, "companyName")} type="text" id="companyName" />
					</div>

					<CRow className="mb-4">
						<ToastContainer />
					</CRow>
					<CButton onClick={() => onSubmit()} className="mb-3 submitLogin">
						{t('register.submit')} &nbsp; {loadingSpinner}
					</CButton>

					<hr />
					<div className="text-center mt-1">
						<p className="mb-0">{t('register.already-registered')}</p>
						<NavLink to="/login" className="registerLink">{t('register.login-here')}</NavLink>
					</div>
				</CForm>
			</div>
		</CContainer>
	);
}

export default Register;
