import React, { useEffect, useState } from "react"
import { CAlert, CCol, CRow, CSpinner } from "@coreui/react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { VictoryChart, 
  VictoryAxis, 
  VictoryBar, 
  VictoryGroup,
  VictoryLabel, 
  VictoryLegend,
  VictoryLine, 
  VictoryPie, 
  VictoryScatter,
  VictoryStack,
  VictoryTooltip
} from "victory";
import { SessionApi } from "../api/SessionApi";
import { DateUtils } from "../utils/DateUtils";

const CHART_COLOR_USER = "#C43228"
const CHART_COLOR_OTHERS = "#4AAE49"

const Statistics = () => {

  const { t } = useTranslation()
  const dispatch = useDispatch();

  const user = useSelector((state: any) => state.auth.user);

  const [loadingData, setLoadingData] = useState(true);
  const [showAlertBuyPlan, setShowAlertBuyPlan] = useState(false)
  const [sessionsCurrent, setSessionsCurrent] = useState({user: 0, others: 0, period_start_date: ''});
  const [sessionsLast, setSessionsLast] = useState({user: 0, others: 0});
  const [contactsPerDay, setContactsPerDay] = useState({user: 0, others: 0});
  const [avgLoggedTime, setAvgLoggedTime] = useState({user: {average: 0, loggedPerDay: {}}, others: {average: 0, loggedPerDay: {}}});

  useEffect(() => {
    let isMounted = true;
    (async () => {
      try {
        if( user && user.plan.name ) {
          const sessCurrent = await SessionApi.getSessionsCurrentPeriod()
          const sessLast = await SessionApi.getSessionsLastPeriod()
          const avgContacts = await SessionApi.getAvgContactsPerDay()
          const avgLoggedTime = await SessionApi.getLoggedTime()
          if(isMounted) {
            setSessionsCurrent(sessCurrent)
            setSessionsLast(sessLast)
            setContactsPerDay(avgContacts)
            setAvgLoggedTime(avgLoggedTime)
            setLoadingData(false)
          }
        } else {
          setShowAlertBuyPlan(true)
          setLoadingData(false)
        }
      } catch (error) {
        dispatch({ type: 'set', errorMessage: error });
        dispatch({ type: 'set', showError: true });
      }
    })()
    return () => { isMounted = false };
  }, []);

  const dataUserApi = {
    name: user ? user.company.name : '',
    currentPeriodStartDate : sessionsCurrent.period_start_date,
    enteredCurrentMonth : sessionsCurrent.user ? sessionsCurrent.user : 0,
    enteredLastMonth : sessionsLast.user ? sessionsLast.user : 0,
    avgContactedLeads : contactsPerDay.user ? contactsPerDay.user : 0,
    avgTimeDay : avgLoggedTime.user.average ? avgLoggedTime.user.average : 0,
    loggedTime : avgLoggedTime.user.loggedPerDay ? avgLoggedTime.user.loggedPerDay : {}
  }

  const dataAverageApi = {
    name: t('statistics.avg-other-companies'),
    enteredCurrentMonth : sessionsCurrent.others ? sessionsCurrent.others : 0,
    enteredLastMonth : sessionsLast.others ? sessionsLast.others : 0,
    avgContactedLeads : contactsPerDay.others ? contactsPerDay.others : 0,
    avgTimeDay : avgLoggedTime.others.average ? avgLoggedTime.others.average : 0,
    loggedTime : avgLoggedTime.others.loggedPerDay ? avgLoggedTime.others.loggedPerDay : {}
  }

  const chartMax = (value1, value2, maxdef, sumToNewMax) => {
    if( value1<=maxdef && value2<=maxdef ) {
      return maxdef;
    } else {
      return Math.max(value1, value2)+sumToNewMax;
    }
  }

  const transformDataCurrentMonth = (dataUser, dataOthers) => {
    const maxValue = chartMax(dataUser, dataOthers, 100,10);
    return [
      [
        {x: "user", y: dataUser},
        {x: "others", y: 0}
      ],
      [
        {x: "user", y: 0},
        {x: "others", y: dataOthers}
      ],
      [
        {x: "user", y: maxValue-dataUser},
        {x: "others", y: maxValue-dataOthers}
      ]
    ];
  }

  const transformDataLastMonth = (data) => {
    const maxValue = data>100 ? data : 100;
    return [
      { x: 1, y: maxValue-data },
      { x: 2, y: data }, 
    ]
  }

  const transformDataContactedLeads = (dataUser, dataOthers) => {
    const maxValue = chartMax(dataUser, dataOthers, 10, 2);
    return [
      [
        {x: "others", y: 0},
        {x: "user", y: dataUser}
      ],[
        {x: "others", y: dataOthers},
        {x: "user", y: 0}
      ],[
        {x: "others", y: maxValue-dataOthers},
        {x: "user", y: maxValue-dataUser}
      ]
    ]
  }

  const transformDataTime = ( data ) => {
    let array = [];
    if( data ) {
      let keys: string[] = Object.keys(data),
      values: number[] = Object.values(data);
      
      for(let i=0 ; i<keys.length && i<values.length ; i++){
          const obj = {x: '0', y: 0};
          const day = keys[i].split('/')[0];
          obj.x = day;
          obj.y = values[i]
          array.push(obj);
      }
    }
    return array;
  }

  const dataVisitsCurrentMonth = transformDataCurrentMonth(dataUserApi.enteredCurrentMonth, dataAverageApi.enteredCurrentMonth)
  const dataVisitsLastMonthUser = transformDataLastMonth(dataUserApi.enteredLastMonth)
  const dataVisitsLastMonthOthers = transformDataLastMonth(dataAverageApi.enteredLastMonth)
  const dataContactedLeads = transformDataContactedLeads(dataUserApi.avgContactedLeads, dataAverageApi.avgContactedLeads)
  const dataTimeDayUser = transformDataTime(dataUserApi.loggedTime)
  const dataTimeDayOthers = transformDataTime(dataAverageApi.loggedTime)

  const buyPlan = () => {
    dispatch({ type: 'plans', showPlans: true });
  }

  return (
    <>
    {loadingData ?
      <div className="d-flex justify-content-center">
        <CSpinner component="span" aria-hidden="true" color="warning" />
      </div>
      :
    <>
    {showAlertBuyPlan ?
      <CAlert color="danger" className="alertNoLeads">{t('statistics.no-plan')} <b className="isLink underline" onClick={buyPlan}>{t('header.buy-plan')}</b></CAlert>
    : 
    <>
    <CRow>
      <CCol xl={7} className="me-4">
        <h4 className="pb-2">{t('statistics.times-period')}</h4>
        <div className="ps-2 d-flex flex-row bgwhite">
          <div className="barContainer">
            <VictoryChart height={300} width={250}
              domainPadding={{ x: 20, y: 0 }}
            >
              <VictoryStack
                colorScale={[CHART_COLOR_USER, CHART_COLOR_OTHERS, "#D3D5D3"]}
              >
                {dataVisitsCurrentMonth.map((data, i) => {
                  return <VictoryBar data={data} key={i}
                            barWidth={30} />;
                })}
              </VictoryStack>
              <VictoryAxis dependentAxis
                tickFormat={(tick) => `${tick}`}
                style={{ 
                  axis: {stroke: "transparent"},
                  tickLabels: {fontSize: 10, padding: 0, fill: "#959797"}
                }}
              />
              <VictoryAxis
                tickFormat={[dataUserApi.enteredCurrentMonth.toString(), dataAverageApi.enteredCurrentMonth.toString()]}
                orientation="top"
                style={{ 
                  axis: {stroke: "transparent"},
                  tickLabels: {fontSize: 20, padding: 5, fill: CHART_COLOR_USER} }}
              />
              <VictoryLegend x={0} y={250}
                  standalone={false}
                  orientation="horizontal"
                  itemsPerRow={1}
                  style={{ labels: {fontSize: 15 } }}
                  data={[
                    { name: t('statistics.current-month'), symbol: {fill: "transparent"} }
                  ]}
                />
            </VictoryChart>
          </div>
          <div className="pieContainer me-2">
            <CRow>
            <CCol>
              <svg viewBox="0 0 400 400" width="100%" height="100%">
                <VictoryLegend x={0} y={100}
                  standalone={false}
                  orientation="horizontal"
                  itemsPerRow={1}
                  style={{ labels: {fontSize: 23 } }}
                  data={[
                    { name: dataUserApi.name, symbol: { fill: CHART_COLOR_USER, type: "square" } }
                  ]}
                />
                <VictoryLegend x={0} y={320}
                  standalone={false}
                  orientation="horizontal"
                  itemsPerRow={1}
                  style={{ labels: {fontSize: 27 } }}
                  data={[
                    { name: t('statistics.last-month'), symbol: {fill: "transparent"} }
                  ]}
                />
                <VictoryLegend x={92} y={377}
                  standalone={false}
                  orientation="horizontal"
                  itemsPerRow={1}
                  style={{ labels: {fontSize: 27 } }}
                  symbolSpacer={8}
                  data={[
                    { name: Math.abs(dataUserApi.enteredCurrentMonth-dataUserApi.enteredLastMonth).toString(), symbol: { fill: CHART_COLOR_USER, type: dataUserApi.enteredCurrentMonth>dataUserApi.enteredLastMonth?"triangleUp":"triangleDown" } }
                  ]}
                />
                <circle cx={115} cy={250} r={72} fill={CHART_COLOR_USER}/>
                <VictoryPie
                  standalone={false}
                  width={230} height={500}
                  data={dataVisitsLastMonthUser}
                  innerRadius={60}
                  cornerRadius={25}
                  labels={() => null}
                  style={{
                    data: { fill: ({ datum }) => {
                      const color = "white";
                      return datum.x === 2 ? color : "transparent";
                    }
                    }
                  }}
                />
                <VictoryLabel
                  textAnchor="middle" verticalAnchor="middle"
                  x={115} y={250}
                  text={dataUserApi.enteredLastMonth.toString()}
                  style={{ fontSize: 45, fill: "white" }}
                />
              </svg>
            </CCol>
            <CCol>
              <svg viewBox="0 0 400 400" width="100%" height="100%">
                <VictoryLegend x={0} y={100}
                  standalone={false}
                  orientation="horizontal"
                  itemsPerRow={1}
                  style={{ labels: {fontSize: 23 } }}
                  data={[
                    { name: dataAverageApi.name, symbol: { fill: CHART_COLOR_OTHERS, type: "square" } }
                  ]}
                />
                <VictoryLegend x={0} y={320}
                  standalone={false}
                  orientation="horizontal"
                  itemsPerRow={1}
                  style={{ labels: {fontSize: 27 } }}
                  data={[
                    { name: t('statistics.last-month'), symbol: {fill: "transparent"} }
                  ]}
                />
                <VictoryLegend x={92} y={377}
                  standalone={false}
                  orientation="horizontal"
                  itemsPerRow={1}
                  style={{ labels: {fontSize: 27 } }}
                  symbolSpacer={8}
                  data={[
                    { name: Math.abs(dataAverageApi.enteredCurrentMonth-dataAverageApi.enteredLastMonth).toString(), symbol: { fill: CHART_COLOR_OTHERS, type: dataAverageApi.enteredCurrentMonth>dataAverageApi.enteredLastMonth?"triangleUp":"triangleDown" } }
                  ]}
                />
                <circle cx={115} cy={250} r={72} fill={CHART_COLOR_OTHERS}/>
                <VictoryPie
                  standalone={false}
                  width={230} height={500}
                  data={dataVisitsLastMonthOthers}
                  innerRadius={60}
                  cornerRadius={25}
                  labels={() => null}
                  style={{
                    data: { fill: ({ datum }) => {
                      const color = "white";
                      return datum.x === 2 ? color : "transparent";
                    }
                    }
                  }}
                />
                <VictoryLabel
                  textAnchor="middle" verticalAnchor="middle"
                  x={115} y={250}
                  text={dataAverageApi.enteredLastMonth.toString()}
                  style={{ fontSize: 45, fill: "white" }}
                />
              </svg>
            </CCol>
            </CRow>
          </div>
        </div>
        <p className="bgwhite ps-4 mb-0">{t('statistics.init-period-date', { date: DateUtils.formatLocaleDate(dataUserApi.currentPeriodStartDate, t('common.date-locale')) }) }</p>
        <p className="bgwhite ps-4 pb-2">{t('statistics.logged-times-explanation-client', { times: Math.abs(dataUserApi.enteredCurrentMonth-dataUserApi.enteredLastMonth).toString(), comparator: (dataUserApi.enteredCurrentMonth>dataUserApi.enteredLastMonth?"más":"menos") } )}</p>
      </CCol>
      <CCol xl={4}>
        <h4 className="pt-sm-0 pt-4 pb-2">{t('statistics.avg-leads')}</h4>
        <div className="d-flex flex-row bgwhite">
          <div className="progressContainer">
          <VictoryChart
            domainPadding={{x: 30}}
          >
            <VictoryLegend x={135} y={23}
              style={{ labels: {fontSize: 23 } }}
              data={[
                { name: t('statistics.leads', { number: dataUserApi.avgContactedLeads }), symbol: { fill: CHART_COLOR_USER, type: "diamond" } }
              ]}
            />
            <VictoryLegend x={75} y={108}
              orientation="horizontal"
              itemsPerRow={1}
              style={{ labels: {fontSize: 15 } }}
              data={[
                { name: dataUserApi.name, symbol: { fill: CHART_COLOR_USER, type: "square" } },
                { name: dataAverageApi.name, symbol: { fill: CHART_COLOR_OTHERS, type: "square" } }
              ]}
            />
            <VictoryStack
              colorScale={[CHART_COLOR_USER, CHART_COLOR_OTHERS, "#D3D5D3"]}
            >
              {dataContactedLeads.map((data, i) => {
                return <VictoryBar horizontal data={data} key={i}
                          barWidth={8} />;
              })}
            </VictoryStack>
            <VictoryLegend x={135} y={235}
              style={{ labels: {fontSize: 23 } }}
              data={[
                { name: t('statistics.leads', { number: dataAverageApi.avgContactedLeads }), symbol: { fill: CHART_COLOR_OTHERS, type: "diamond" } }
              ]}
            />
            <VictoryAxis dependentAxis
              standalone={false}
              tickFormat={[0, chartMax(dataUserApi.avgContactedLeads, dataAverageApi.avgContactedLeads, 10, 2)]}
              style={{ 
                axis: {stroke: "transparent"},
                tickLabels: {fontSize: 15, padding: -20, fill: "#D3D5D3"} }}
            />
          </VictoryChart>
          </div>
        </div>
      </CCol>
    </CRow>
    <CRow>
      <CCol>
        <h4 className="pt-4 pb-2">{t('statistics.avg-time')}</h4>
        <div className="d-flex flex-row bgwhite pt-4 pb-4 mb-4">
          <div className="lineChartContainer">
            <VictoryChart minDomain={{ y: 0 }}
            >
              <VictoryLegend x={75} y={0}
                centerTitle
                orientation="horizontal"
                itemsPerRow={1}
                style={{ labels: {fontSize: 15 } }}
                data={[
                  { name: dataUserApi.name, symbol: { fill: CHART_COLOR_USER, type: "square" } },
                ]}
              />
              <VictoryLegend x={395} y={125}
                style={{ labels: {fontSize: 25 } }}
                data={[
                  { name: t('statistics.hours-symbol', { number: dataUserApi.avgTimeDay }), symbol: { fill: CHART_COLOR_USER, type: "diamond" } }
                ]}
              />
              <VictoryAxis
                tickFormat={(x) => `${x%2===0 ? x : ''}`}
                style={{
                  ticks: {stroke: "#D3D5D3", size: 5},
                }} />
              <VictoryAxis dependentAxis
                tickValues={[0, 2.5, 5]}
                label={t('statistics.hours')}
                style={{
                  axis: {stroke: "transparent"},
                  grid: {stroke: "#D3D5D3"},
                  axisLabel: {fontSize: 15, padding: 35},
                }} />
              <VictoryGroup
                color={CHART_COLOR_USER}
                labels={({ datum }) => t("statistics.day")+` ${datum.x}: ${datum.y} `+t("statistics.hours-lowercase")}
                labelComponent={
                  <VictoryTooltip
                    style={{ fontSize: 11 }}
                  />
                }
                data={dataTimeDayUser}
              >
                <VictoryLine />
                <VictoryScatter
                  size={({ active }) => active ? 8 : 3}
                />
              </VictoryGroup>
            </VictoryChart>
          </div>
          <div className="lineChartContainer">
            <VictoryChart minDomain={{ y: 0 }}
            >
              <VictoryLegend x={75} y={0}
                centerTitle
                orientation="horizontal"
                itemsPerRow={1}
                style={{ labels: {fontSize: 15 } }}
                data={[
                  { name: dataAverageApi.name, symbol: { fill: CHART_COLOR_OTHERS, type: "square" } },
                ]}
              />
              <VictoryLegend x={395} y={125}
                style={{ labels: {fontSize: 25 } }}
                data={[
                  { name: t('statistics.hours-symbol', { number: dataAverageApi.avgTimeDay }), symbol: { fill: CHART_COLOR_OTHERS, type: "diamond" } }
                ]}
              />
              <VictoryAxis
                tickFormat={(x) => `${x%2===0 ? x : ''}`}
                style={{
                  ticks: {stroke: "#D3D5D3", size: 5},
                }} />
              <VictoryAxis dependentAxis
                tickValues={[0, 2.5, 5]}
                label={t('statistics.hours')}
                style={{
                  axis: {stroke: "transparent"},
                  grid: {stroke: "#D3D5D3"},
                  axisLabel: {fontSize: 15, padding: 35},
                }} />
              <VictoryGroup
                color={CHART_COLOR_OTHERS}
                labels={({ datum }) => t("statistics.day")+` ${datum.x}: ${datum.y} `+t("statistics.hours-lowercase")}
                labelComponent={
                  <VictoryTooltip
                    style={{ fontSize: 11 }}
                  />
                }
                data={dataTimeDayOthers}
              >
                <VictoryLine />
                <VictoryScatter
                  size={({ active }) => active ? 8 : 3}
                />
              </VictoryGroup>
            </VictoryChart>
          </div>
        </div>
      </CCol>
    </CRow>
    </>
    }
    </>
  }
  </>
  )
}

export default Statistics