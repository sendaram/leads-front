import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useDispatch } from 'react-redux'
import { LeadsApi } from '../api/LeadsApi'
import { LeadsDataTableAdmin } from '../components/LeadsDataTableAdmin'
import { CImage } from '@coreui/react'
import { CCol, CFormSelect, CRow, CSpinner } from '@coreui/react';
import imgGreenContacts from '../assets/images/green-contacts.png'
import imgOrangeContacts from '../assets/images/orange-contacts.png'
import imgRedContacts from '../assets/images/red-contacts.png'
import { meCall } from '../api/endpoints'
import { UserApi } from '../api/UserApi'

const PanelAdmin = () => {

  const { t } = useTranslation()
  const dispatch = useDispatch();

  const [ leadsList, setLeadsList ] = useState([]);
  const [ loading, setLoading ] = useState(true);
  const [ reloadLeads, setReloadLeads ] = useState(true);
  const [selectedUser, setSelectedUser] = useState("");
  const [displayingUserLeads, setDisplayingUserLeads] = useState(""); //Username whose leads are being displayed
  const [optionsUsers, setOptionsUsers] = useState(null);
  const [loadingUser, setLoadingUser] = useState(false);
  const [loadingData, setLoadingData] = useState(true);

  useEffect(() => {
    if(reloadLeads) {
      (async () => {
        try {
          if(selectedUser) {
            const data = await await LeadsApi.getLeadsOfUser(selectedUser);
            setLeadsList(data);
          } else {
            setLeadsList([]);
          }
          setReloadLeads(false)
          setLoading(false);
        } catch (error) {
          dispatch({ type: 'set', errorMessage: error });
          dispatch({ type: 'set', showError: true });
        }
      })()
    }
  }, [reloadLeads])

  useEffect(() => {
    let isMounted = true;
    (async () => {
      try {
        // @MF TODO: revisar aixó
        if( selectedUser) {
          const userLeads = await LeadsApi.getLeadsOfUser(selectedUser);
          
          if( isMounted ) {
            setLeadsList(userLeads);
            setDisplayingUserLeads(selectedUser === '-1' ? 'TODOS' : (optionsUsers? optionsUsers.map((val) => {return val.value===selectedUser ? val.label : null}):''))
          }
        } else {
          if( isMounted ) {
            setDisplayingUserLeads(null)
          }
          // reset table
        }
        if( isMounted ) {
          setLoadingUser(false)
        }
      } catch (error) {
        // @MF TODO: translate error
        dispatch({ type: 'set', errorMessage: error });
        dispatch({ type: 'set', showError: true });
      }
    })()
    return () => { isMounted = false };
  }, [selectedUser]);

  useEffect(() => {
    let isMounted = true;
    (async () => {
      try {
        await meCall();
      } catch (error) {
        console.log(' me error: ', error);
      }
      try {
        const userLeads = await LeadsApi.getLeadsOfUser('-1');
        if( isMounted ) {
          setLeadsList(userLeads);
          const usersList = await UserApi.getAllUsers();
          const formattedList = formatUserOptions(usersList);
          setOptionsUsers(formattedList);
          setLoadingData(false)
        }
      } catch (error) {
        dispatch({ type: 'set', errorMessage: t(error) });
        dispatch({ type: 'set', showError: true });
      }
    })()
    return () => { isMounted = false };
  }, []);

  
  const formatUserOptions = (list) => {
    let formatted = [{label: t('statistics.select-client-list'), value: -1}];
    list.map((user) => {
      formatted.push( {value : user.id, label : user.company.name+" ("+user.name+")"});
    })
    return formatted;
  }

  const onChangeSelectUser = (e) => {
    setLoadingUser(true);
    setSelectedUser(e.target.options[e.target.selectedIndex].value);
  }

  return (
    <>
    {loadingData ?
          <div className="d-flex justify-content-center">
            <CSpinner component="span" aria-hidden="true" color="warning" />
          </div>
          :
          <>
          <h1 className="mb-3">{t('admin.user-leads-display')}: {displayingUserLeads}</h1>
          
          <CRow className="bgwhite pt-3 pb-2 mb-3">
                <CCol xl={2}><h6>{t('statistics.select-client')}</h6></CCol>
                <CCol xl={4}><CFormSelect name="user" size="sm" value={selectedUser} disabled={loadingUser}
                        options={optionsUsers} onChange={e => onChangeSelectUser(e)} /></CCol>
                <CCol xl={1}>{loadingUser ? <CSpinner component="span" aria-hidden="true" size="sm" color="warning" />:''}</CCol>
              </CRow>

          <LeadsDataTableAdmin setData={setLeadsList} data={leadsList} loading={loading} setReload={setReloadLeads}/> 

          <div className="mt-4 ms-5 contactsLegend">
            <h6>{t('panel.ncontacts')}</h6>
            <ul className="ps-0">
              <li><CImage src={imgGreenContacts} className="align-text-top" /> 1-2</li>
              <li><CImage src={imgOrangeContacts} className="align-text-top" /> 3</li>
              <li><CImage src={imgRedContacts} className="align-text-top" /> 4</li>
            </ul>
          </div>
          </>
      }
      </>
  )
}

export default PanelAdmin