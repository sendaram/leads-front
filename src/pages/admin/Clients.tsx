import { CCol, CContainer, CRow } from '@coreui/react';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { AdminApi } from '../../api/AdminApi';
import { meCall } from '../../api/endpoints';
import { UserApi } from '../../api/UserApi';
import AdminUsersDataTable from '../../components/AdminUsersDataTable';

const Clients = () => {

  const { t } = useTranslation()
  const dispatch = useDispatch();

  const [ usersList, setUsersList ] = useState([]);
  const [ loading, setLoading ] = useState(true);
  const [ reload, setReload ] = useState(true)
  const [userInformation, setUserInformation] = useState(null);
  const [loadingInformation, setLoadingInformation ] = useState(true);

  useEffect(() => {
    meCall();
    let isMounted = true;
    (async () => {
      try {
        setLoadingInformation(true);
        const userInfo = await AdminApi.getUserAllStatistics();
        console.log('userInfo: ', userInfo);
        // const allInvoices = await PlanApi.getInvoices();
        // allInvoices.sort(compare);
        
        if(isMounted) {
          setUserInformation(userInfo);
          setLoadingInformation(false);
        }
      } catch (error) {
        dispatch({ type: 'set', errorMessage: error });
        dispatch({ type: 'set', showError: true });
      }
      
    })()
    return () => { isMounted = false };
  }, []);


  useEffect(() => {
    if(reload) {
      (async () => {
        try {
          const data = await UserApi.getAllUsers();
          setUsersList(data);
          setReload(false)
          setLoading(false);
        } catch (error) {
          dispatch({ type: 'set', errorMessage: error });
          dispatch({ type: 'set', showError: true });
        }
      })()
    }
  }, [reload])

  return(
    <>
      
      { !loadingInformation && userInformation ?
        <CContainer sm className="accountContainer pt-3 ps-3 pe-3 pb-3 bgwhite">
          <CRow className="mb-4">
          <CCol>
            <h4>{'Estadísticas generales'}</h4>
            <div className="accountBox">
              <CRow>
                  <CCol lg={9} className="accountGroup">
                    <p>{'Usuarios totales'}: {userInformation.allUsers}</p>
                    <p>{'Registrados los últimos 30 dias'}:{userInformation.newUsers}</p>
                  </CCol>
              </CRow>
            </div>
          </CCol>
        </CRow>
        </CContainer>
        : 
        <></>
      }
      <h1>{t('admin.users-list')}</h1>
      <AdminUsersDataTable setData={setUsersList} data={usersList} loading={loading} setReload={setReload}/>
    </>
  )
}

export default Clients