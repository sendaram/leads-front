import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { AdminApi } from '../../api/AdminApi';
import AdminNotificationsDatatable from '../../components/admin/AdminNotificationsDatatable';

const Report = () => {
  const { t } = useTranslation()
  const dispatch = useDispatch();

  const [ usersList, setUsersList ] = useState([]);
  const [ loading, setLoading ] = useState(true);
  const [ reload, setReload ] = useState(true)

  useEffect(() => {
    if(reload) {
      (async () => {
        try {
          const data = await AdminApi.getAdminNotifications();
          setUsersList(data);
          setReload(false)
          setLoading(false);
        } catch (error) {
          dispatch({ type: 'set', errorMessage: error });
          dispatch({ type: 'set', showError: true });
        }
      })()
    }
  }, [reload]);

  useEffect(() => { 
    let isMounted = true;
    (async () => {
      if(isMounted) {
        try {
          const data = await AdminApi.getAdminNotifications();
          setUsersList(data);
          setReload(false)
          setLoading(false);
        } catch (error) {
          dispatch({ type: 'set', errorMessage: error });
          dispatch({ type: 'set', showError: true });
        }
      }
    })();
    return () => { isMounted = false };
  }, []) ;

  return(
    <>
      <h1>{t('admin.notifications-list')}</h1>
      <AdminNotificationsDatatable setData={setUsersList} data={usersList} loading={loading} setReload={setReload}/>
    </>
  )
}

export default Report