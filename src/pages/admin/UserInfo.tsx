import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useSelector, useDispatch } from 'react-redux';
import makeAnimated from 'react-select/animated';
// import { DateUtils } from '../utils/DateUtils';
import { CCol, 
  CContainer,
  CRow,
  CSpinner
 } from '@coreui/react'
 
import { AdminApi } from '../../api/AdminApi';
import { useParams } from 'react-router-dom';
import ActionsDatatable from '../../components/ActionsDatatable';

const UserInfo = () => {

  const { t } = useTranslation()
  const dispatch = useDispatch();
  const user = useSelector((state: any) => state.auth.user);
  const [userActions, setUserActions] = useState([]);
  let { userId } = useParams()

  //For Multi Select (react-select lib)
  const animatedComponents = makeAnimated();
  
  const [userInformation, setUserInformation] = useState(null);
  const [loadingData, setLoadingData ] = useState(true);

  
  useEffect(() => {
    let isMounted = true;
    (async () => {
      try {
        const userInfo = await AdminApi.getUserInfo(userId);
        const userActions = await AdminApi.getAdminUserActions(userId);
        console.log('user actions: ', userActions)
        // const allInvoices = await PlanApi.getInvoices();
        // allInvoices.sort(compare);
        
        if(isMounted) {
          setUserInformation(userInfo);
          setUserActions(userActions);
          setLoadingData(false);
        }
      } catch (error) {
        dispatch({ type: 'set', errorMessage: error });
        dispatch({ type: 'set', showError: true });
      }
      
    })()
    return () => { isMounted = false };
  }, []);

  return (
    <>
    {loadingData || !userInformation ?
        <div className="d-flex justify-content-center">
          <CSpinner component="span" aria-hidden="true" color="warning" />
        </div>
      :
    <>
      <CContainer sm className="accountContainer pt-3 ps-3 pe-3 pb-3 bgwhite">
        <CRow className="mb-4">
        <CCol>
          <h4>{t('my-account.enterprise-info')}</h4>
          <div className="accountBox">
            <CRow>
                <CCol lg={9} className="accountGroup">
                  <p>{userInformation.name}</p>
                  { userInformation.company.cif ?
                    <><p>{t('my-account.cif')}: {userInformation.company.cif}</p>
                    <p>{userInformation.company.street}</p>
                    <p>{userInformation.company.cp} {userInformation.company.city}</p>
                    <p>{userInformation.company.country}</p>
                    <p>{userInformation.company.email}</p></>
                  : <p className='info-edit-company'>{t('user-info.complete-company-info')}</p>
                  }
                </CCol>
            </CRow>
          </div>
        </CCol>
      </CRow>
      <CRow className="mb-3">
          <h4>Acciones</h4>
          <ActionsDatatable setData={null} data={userActions} loading={false} setReload={null}/>
      </CRow>
      </CContainer>
    </>
    }
  </>
  )
}

export default UserInfo;