import { LOGOUT_REQUEST } from "../contexts/actions/userActions";
import store from "../contexts/store";
import { authApiCall } from "./apiCalls";


const getDashboard = (): Promise<any> => {
	return new Promise((resolve, reject) => {
		authApiCall.get("/statistics/dashboard")
			.then(({ data, status }) => {
				resolve(data);
			})
			.catch((error) => {
				if (error.response) {
                    const serverError = error.response.data;
                    if(serverError.statusCode === 403) {
                        reject('not logged');
                        store.dispatch({type: LOGOUT_REQUEST.type});
                    }
                    reject(serverError.message);
                } else if (error.request) {
                    reject('Server not responds.');
                } else {
                    console.log('StatisticsApi.getDashboard error', error.message);
                }
			})
	});
}


export const StatisticsApi = {
    getDashboard
  };