import { LOGOUT_REQUEST } from "../contexts/actions/userActions";
import store from "../contexts/store";
import { authApiCall } from "./apiCalls";

/** Recupera los leads dados de alta para el usuario */
const getUserInfo = (userId: string): Promise<any> => {
	return new Promise((resolve, reject) => {
		const params = { id: userId };
		authApiCall.get("/admin/userInfo", { params })
			.then(({ data, status }) => { resolve(data) })
			.catch((error) => { reject(processError(error)) })
	});
}

// statistics for one user
const getUsersStatistics = (userId: string): Promise<any> => {
    const params = { id: userId };
	return new Promise((resolve, reject) => {
		authApiCall.get("/admin/user/statistics", {params})
			.then(({ data, status }) => { resolve(data) })
			.catch((error) => { reject(processError(error)) })
	});
}

// statistics for all users
const getUserAllStatistics = (): Promise<any> => {
	return new Promise((resolve, reject) => {
		authApiCall.get("/admin/user/allstatistics")
			.then(({ data, status }) => { resolve(data) })
			.catch((error) => { reject(processError(error)) })
	});
}

const getAdminLeadActions = (id): Promise<any> => {
	return new Promise((resolve, reject) => {
        const params = { id };
		authApiCall.get("/admin/lead/actions", {params})
        .then(({ data, status }) => { resolve(data) })
        .catch((error) => { reject(processError(error)) })
	});
}

const getAdminUserActions = (id): Promise<any> => {
	return new Promise((resolve, reject) => {
        const params = { id };
		authApiCall.get("/admin/user/actions", {params})
        .then(({ data, status }) => { resolve(data) })
        .catch((error) => { reject(processError(error)) })
	});
}

const getAdminNotifications = (): Promise<any> => {
	return new Promise((resolve, reject) => {
		authApiCall.get("/admin/notifications")
        .then(({ data, status }) => { resolve(data) })
        .catch((error) => { reject(processError(error)) })
	});
}

const processError = (error: any) => {
    if (error.response) {
        const serverError = error.response.data;
        if(serverError.statusCode === 403) {
            store.dispatch({type: LOGOUT_REQUEST.type});
            return 'error.0003';
        }
		else if(serverError.statusCode === 500) {
            return 'error.0001';
        }
		else if(serverError.statusCode === 400) {
            return 'error.'+serverError.message;
        }
        return serverError.message;
    } else if (error.request) {
        return 'error.0002';
    } else {
        return 'error.0001';
    }
}

export const AdminApi = {
    getUserInfo,
    getAdminLeadActions,
    getAdminUserActions,
    getAdminNotifications,
    getUsersStatistics,
    getUserAllStatistics
  };