import store from "../contexts/store";
import { authApiCall } from "./apiCalls";
import { LOGOUT_REQUEST } from "../contexts/actions/userActions";

const getAll = (): Promise<any> => {
	return new Promise((resolve, reject) => {
		authApiCall.get("/segmento/all")
			.then(({ data, status }) => {
				resolve(data);
			})
			.catch((error) => {
				if (error.response) {
                    const serverError = error.response.data;
                    if(serverError.statusCode === 403) {
                        reject('not logged');
                        store.dispatch({type: LOGOUT_REQUEST.type});
                    }
                    reject(serverError.message);
                } else if (error.request) {
                    reject('Server not responds.');
                } else {
                    console.log('SegmentosApi.getAll error', error.message);
                }
			})
	});
}

const getFromMarca = (marca): Promise<any> => {
    const params = {
        marca
    };
	return new Promise((resolve, reject) => {
		authApiCall.get("/segmento/fromMarca", {params})
			.then(({ data, status }) => {
				resolve(data);
			})
			.catch((error) => {
				if (error.response) {
                    const serverError = error.response.data;
                    if(serverError.statusCode === 403) {
                        reject('not logged');
                        store.dispatch({type: LOGOUT_REQUEST.type});
                    }
                    reject(serverError.message);
                } else if (error.request) {
                    reject('Server not responds.');
                } else {
                    console.log('SegmentosApi.getAll error', error.message);
                }
			})
	});
}

export const SegmentosApi = {
    getAll,
    getFromMarca
  };