import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next'
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom'
import { logoutCall, logoutImpersonate, meCall } from '../../api/endpoints';
import {
  CAlert,
  CContainer,
  CHeader,
  CHeaderNav,
  CImage,
  CNavItem,
  CProgress,
  CProgressBar,
} from '@coreui/react';
import { SessionApi } from '../../api/SessionApi';
import logout from '../../assets/images/logout.png';

const AppHeader = () => {

  const navigate = useNavigate();
  const { t } = useTranslation()
  const dispatch = useDispatch();

  const user = useSelector((state: any) => state.auth.user);

  const [percentage, setPercentage] = useState(0);
  const [remainingLeads, setRemainingLeads] = useState(0);
  const [leadsLimit, setLeadsLimit] = useState(0);

  useEffect(() => {
    if( user ) {
      const leadsToUse = user.plan.leadsRemain ? user.plan.leadsRemain : 0;
      const maxLeads = user.plan.leadsLimit ? user.plan.leadsLimit : 0;
      const percent = 100*leadsToUse/maxLeads;
      setPercentage(Math.round(percent))
      setRemainingLeads(leadsToUse)
      setLeadsLimit(maxLeads)
    }
  }, [user])

  const doLogout = async () => {
      dispatch({ type: 'billing', warningBilling: false })
      closeSession();
      await logoutCall();
  }

  const doImpersonate = async () => {
    await logoutImpersonate();
    navigate("/clients");
    window.location.reload();
}

  const closeSession = async () => {
    const sessionId = localStorage.getItem('sessionID');
    try {
      await SessionApi.closeSession(sessionId, new Date());
    } catch (error) {
      
    }
		
    localStorage.removeItem('sessionID');
    localStorage.removeItem('sessionTSTP');
	}

  const buyPlan = () => {
    dispatch({ type: 'plans', showPlans: true });
  }

  const buyProduct = () => {
    dispatch({ type: 'products', showProducts: true });
  }

  const getProgressBarColor = (percentage = 0) => {
    let color = "info";
    if( percentage >= 80 ) {
      color = "success"
    } else if( percentage <= 20 ) {
      color = "danger"
    }
    return color;
  }

  return (
    <>
    { user ?
      <CHeader position="sticky" className="mb-2">
        <CContainer fluid>
          { user.role === 'user' ?
            <CHeaderNav>
              <CNavItem className="navitemRemainingLeads">
                <div className="text-center bgblue remainingLeadsBox">
                  <p className="mb-0">{t('header.remaining')}:</p>
                  <p className="mb-0 remainingLeads">{t('header.range', { used: remainingLeads, total: leadsLimit })}</p>
                </div>
                { user.plan.extraLeads>0 ?
                  <p className='mb-0 dashboardValue'>{t('header.extra-leads', { qtty: user.plan.extraLeads })}</p>
                  :''
                }
              </CNavItem>
              { //Si le quedan leads, mostrar barra de progreso
                percentage>0 ?
                <><CNavItem className="ms-4 navitemProgressbar">
                  <CProgress>
                      <CProgressBar value={percentage} color={getProgressBarColor(percentage)}> 
                        {percentage} % 
                      </CProgressBar>  
                    </CProgress>
                  </CNavItem>
                  {
                    user.permissions.indexOf("EXTRA_LEADS") != -1
                      ? <CNavItem className="ms-4 navitemBuyLeads">
                          <CAlert color="succes" onClick={buyProduct}>{t('header.buy-leads')}</CAlert>
                        </CNavItem>
                      : <></>
                  }
                </>
              : //Si no le quedan leads:
                <CNavItem className="ms-4">
                  { //¿Tiene plan contratado?
                    user.plan.name 
                    ? user.plan.extraLeads>0 //Mostrar alert: ExtraLeads o Comprar leads
                        ? <CAlert color="danger" className="alertNoLeads">{t('header.no-plan-leads-yes-extra', {number: user.plan.extraLeads})}</CAlert>
                        : <CAlert color="danger" className="alertNoLeads">{t('header.no-remaining-leads')} <b className="isLink underline" onClick={buyProduct}>{t('header.buy-leads')}</b></CAlert>
                    : //Mostrar alert: Comprar plan
                      <CAlert color="danger" className="alertNoLeads">{t('header.no-plan')} <b className="isLink underline" onClick={buyPlan}>{t('header.buy-plan')}</b></CAlert>
                  }
                </CNavItem>
              //END percentage>0 ?
              }
            </CHeaderNav>
            : ''
          //END user.role === 'user'
          }
        <CHeaderNav className="ms-auto">
          <CNavItem className="navitemWelcome">
            <p>{t('header.welcome')} | <b>{user.company.name}</b></p>
          </CNavItem>
          {
            user && user.impersonating ?
            <CNavItem className="ms-4 navitemLogout">
              <CImage src={logout} width={18} height={18} className="isLink" onClick={doImpersonate} 
                    alt="logout" title={t('header.impersonate')} />
            </CNavItem>
          :
            <CNavItem className="ms-4 navitemLogout">
              <CImage src={logout} width={18} height={18} className="isLink" onClick={doLogout} 
                    alt="logout" title={t('header.logout')} />
            </CNavItem>
          }
          
        </CHeaderNav>
      </CContainer>
    </CHeader>
    : ''
  }</>
  )
}

export default AppHeader;