import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { CImage,
  CSidebar, 
  CSidebarBrand, 
  CSidebarNav } from '@coreui/react'

import { AppSidebarNav } from './AppSidebarNav'

import logo from '../../assets/images/spotileads-horizontal.png'

import SimpleBar from 'simplebar-react'
import 'simplebar/dist/simplebar.min.css'

// sidebar nav config
import navigation from './_nav'
import { AppState } from '../../interfaces/Store'

const AppSidebar = () => {
  const dispatch = useDispatch()
  
  const unfoldable = useSelector((state: AppState) => state.sidebarUnfoldable)
  const sidebarShow = useSelector((state: AppState) => state.sidebarShow)
  const user = useSelector((state: any) => state.auth.user)
  const warningBilling = useSelector((state: any) => state.changeState.warningBilling);

  const [navItems, setNavItems] = useState(user && user.role === 'admin' ? navigation.adminItems : navigation.userItems);

  useEffect(()=>{
    (async () => {
      try {
        let items = (user && user.role != 'admin') ? navigation.userItems : navigation.adminItems;
        if( warningBilling ) {
          items.map((item)=>{
            if(item.to === '/my-account')
              return item.badge = {color: 'danger', text: '1'};
            else return item;
          })
        } else {
          items.map((item)=>{
            if(item.to === '/my-account')
              return item.badge = null;
            else return item;
          })
        }
        setNavItems([...items]);
      } catch (error) {
        console.log('El error ? ', error)
        dispatch({ type: 'set', errorMessage: error });
        dispatch({ type: 'set', showError: true });
      }
    })()
  }, [warningBilling])

  useEffect(()=>{
    (async () => {
      try {
        let items = (user && user.role != 'admin') ? navigation.userItems : navigation.adminItems;
        setNavItems([...items]);
      } catch (error) {
        console.log('El error ? ', error)
        dispatch({ type: 'set', errorMessage: error });
        dispatch({ type: 'set', showError: true });
      }
    })()
  }, [user])

  return (
    <CSidebar
      position="fixed"
      unfoldable={unfoldable}
      visible={sidebarShow}
    >
      {/* to="/" */}
      <CSidebarBrand className="d-none d-md-flex" > 
        <CImage className="sidebar-brand-full" src={logo} />
      </CSidebarBrand>
      <CSidebarNav>
        <SimpleBar>
          <AppSidebarNav items={navItems} />
        </SimpleBar>
      </CSidebarNav>
    </CSidebar>
  )
}

//React omitirá renderizar el componente y reusará el último resultado renderizado si tiene las mismas props
export default React.memo(AppSidebar)
