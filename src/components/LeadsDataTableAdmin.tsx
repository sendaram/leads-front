import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import DataTable from 'react-data-table-component'
import { useNavigate } from 'react-router-dom'
import { DataTableFilters } from './DataTableFilters'
import { DateUtils} from '../utils/DateUtils'
import { initialFilterValues } from './DataTableFilters'
import { ContactsRate } from './ContactsRate'
import { UserApi } from '../api/UserApi'


export function LeadsDataTableAdmin ({ setData, data, loading, setReload }) {

    const { t } = useTranslation();
    const navigate = useNavigate()

    const [resetPaginationToggle, setResetPaginationToggle] = useState(false);
    const [filterValues, setFilterValues] = useState(initialFilterValues);
    
    //Methods for handle AppFilterComponent, for updating datatable content
    const handleFilterChange = (newValues) => {
      setFilterValues({...newValues})
    }

    const handleClear = () => {
      setResetPaginationToggle(!resetPaginationToggle)
    }

    //Show lead info card
    const openLeadCard = (row, event) => {
        navigate("/card-admin/"+row.id)
    };

    //Const for datatable configuration
    const tableColumns = [
        {
          name: t('datatable.date'),
          selector: row => row.date,
          center: true,
          compact: true,
          sortable:true
        },
        {
          name: t('datatable.name'),
          selector: row => row.name,
          center: true,
          compact: true,
          sortable:true
        },
        {
          name: t('datatable.surname'),
          selector: row => row.surname,
          center: true,
          compact: true,
          sortable:true
        },
        {
          name: t('datatable.brand'),
          selector: row => row.brand,
          center: true,
          compact: true,
          sortable:true
        },
        {
          name: t('datatable.model'),
          selector: row => row.model,
          center: true,
          compact: true,
          sortable:true
        },
        {
          name: t('datatable.segment'),
          selector: row => row.segment,
          center: true,
          compact: true,
          sortable:true
        },
        {
          name: t('datatable.visualizations'),
          selector: row => row.visualizations,
          center: true,
          compact: true,
          sortable:true
        },
        {
          name: t('datatable.ncontacts'),
          selector: row => row.contactsNumber,
          cell: row => <ContactsRate contacts={row.contactsNumber} />,
          center: true,
          compact: true,
        },
        {
          name: t('datatable.province'),
          selector: row => row.province,
          center: true,
          compact: true,
        }
      ]
    
    //Format DB data to DataTable format
    const tableData = data ? data.map((item, index) => (
        {
            id: item.id,
            date: DateUtils.formatLocaleDate(item.fechaAlta, t('common.date-locale')),
            name: item.name,
            surname: item.surname,
            brand: item.brand,
            model: item.model,
            segment: item.type,
            visualizations: item.visualizations,
            contactsNumber: item.contactsNumber,
            province: item.province
        }
    )) : [];

    const paginationOptions = {
      rowsPerPageText: t('datatable.page-rows'),
      rangeSeparatorText: t('datatable.range-separator'),
      selectAllRowsItem: true,
      selectAllRowsItemText: t('datatable.all-rows'),
    }

    const customStyles = {
      headCells: {
        style: {
          color: '#FFFFFF',
          backgroundColor: '#DB8B3C',
        },
      },
      subHeader: {
        style: {
          alignItems: 'start',
          justifyContent: 'start',
        },
      },
    }

    //Dynamic data filtering, managed by DataTable lib
    const filteredItems = tableData.filter(item => {
        
        //If no filter criteria, add to filteredItems
        if(filterValues == null) return true;
        if(filterValues.brand.value==='-1' && filterValues.model.value==='-1' && filterValues.segment.value==='-1' 
            && filterValues.province.value==='-1' && filterValues.date.value==='-1' && filterValues.contacts==='') {
          return true;
        }
        //Filter for each criteria
        if (filterValues.brand.value!=='-1') {
          if(item.brand.toLowerCase().trim() !== filterValues.brand.label.toLowerCase().trim())
            return false;
        }
        if(filterValues.model.value!=='-1') {
          if(item.model.toLowerCase().trim() !== filterValues.model.label.toLowerCase().trim())
            return false;
        }
        if(filterValues.segment.value!=='-1') {
          if(item.segment.toLowerCase().trim() !== filterValues.segment.label.toLowerCase().trim())
            return false;
        }
        if(filterValues.province.value!=='-1') {
          if(!item.province) return false;
          if(item.province && item.province.toLowerCase().trim() !== filterValues.province.label.toLowerCase().trim())
            return false;
        }
        if(filterValues.date.value!=='-1') {
          //Convert dates for comparing Date objects
          const dateParts = item.date.split("/");
          const itemDate = new Date(+dateParts[2], +dateParts[1] - 1, +dateParts[0]);
          const dateParts2 = filterValues.date.value.split("/");
          const filterDate = new Date(+dateParts2[2], +dateParts2[1] - 1, +dateParts2[0]);
          if(itemDate < filterDate)
            return false;
        }
        if (filterValues.contacts!=='-1') {
          if(item.contactsNumber > parseInt(filterValues.contacts.trim()))
            return false;
        }
        return true;
    });
  
    return (
      <DataTable
          pagination
          columns={tableColumns}
          customStyles={customStyles}
          data={filteredItems}
          highlightOnHover={true}
          onRowClicked={openLeadCard}
          paginationComponentOptions={paginationOptions}
          paginationRowsPerPageOptions={[25, 50, 100]}
          paginationResetDefaultPage={resetPaginationToggle}
          noDataComponent={<div className="pb-4">{t('datatable.no-records-to-display')}</div>}
          progressPending={loading}
          subHeader
          subHeaderComponent={<DataTableFilters 
                                setData={setData}
                                handleChange={handleFilterChange} 
                                handleReset={handleClear} />}
      />
    )
}

export default LeadsDataTableAdmin;