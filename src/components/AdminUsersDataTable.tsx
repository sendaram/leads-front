import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import DataTable from 'react-data-table-component'
import { useNavigate } from 'react-router-dom'
import { UserApi } from '../api/UserApi'
import { AdminUserActions } from './AdminUserActions'

export function AdminUsersDataTable({ setData, data, loading, setReload }) {

    const { t } = useTranslation();
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const user = useSelector((state: any) => state.auth.user);

    const EMPTY_LABEL = '';
    const DEFAULT_SELECT_VALUE = '-1';

    const initialFilterValues = {
      id : {label: EMPTY_LABEL, value: DEFAULT_SELECT_VALUE},
      name : {label: EMPTY_LABEL, value: DEFAULT_SELECT_VALUE},
      email : {label: EMPTY_LABEL, value: DEFAULT_SELECT_VALUE}
    }

    const [resetPaginationToggle, setResetPaginationToggle] = useState(false);
    const [filterValues, setFilterValues] = useState(initialFilterValues);

    //Methods for handle AppFilterComponent, for updating datatable content
    const handleFilterChange = (newValues) => {
      setFilterValues({...newValues})
    }

    const handleClear = () => {
      setResetPaginationToggle(!resetPaginationToggle)
    }

    //Show lead info card
    const impersonateUser = async (userId, event) => {
      try {
        await UserApi.impersonateUser(userId);
        navigate("/my-account");
        window.location.reload();
      } catch (error) {
        console.log('Error impersonating: ', error)
      }
      
    };

    const userInfo = (id) => {
      navigate("/userInfo/"+id)
    }

    //Const for datatable configuration
    const tableColumns = [
        {
          name: 'Nombre',
          selector: row => row.name,
          center: true,
          compact: true,
          sortable:true
        },
        {
          name: 'Empresa',
          selector: row => row.empresa,
          center: true,
          compact: true,
          sortable:true
        },
        {
          name: 'Email',
          selector: row => row.email,
          center: true,
          compact: true,
          sortable:true
        },
        {
          name: t('common.phone'),
          selector: row => row.telefono,
          center: true,
          compact: true,
          sortable:true
        },
        {
          name: 'Leads del periodo actual',
          selector: row => row.leadsRemain,
          center: true,
          compact: true,
          sortable:false
        },
        {
          name: 'Leads totales contratados',
          selector: row => row.leadsNumber,
          center: true,
          compact: true,
          sortable:true
        },
        {
          name: '',
          selector: row => row.id,
          cell: row => <AdminUserActions 
                            id={row.id} 
                            impersonate={impersonateUser} 
                            userInfo={userInfo} 
                        />,
          center: true,
          compact: true,
          minWidth: '4rem',
          maxWidth: '4rem'
        }
      ]
    
    //Format DB data to DataTable format
    const tableData = data.map((item, index) => (
        {
            id: item.id,
            name: item.name,
            email: item.email,
            empresa: item.company.name,
            telefono: item.company.phone,
            leadsRemain: item.leadsRemain,
            leadsNumber: item.leadsNumber
        }
    ))

    const paginationOptions = {
      rowsPerPageText: t('datatable.page-rows'),
      rangeSeparatorText: t('datatable.range-separator'),
      selectAllRowsItem: true,
      selectAllRowsItemText: t('datatable.all-rows'),
    }

    const customStyles = {
      headCells: {
        style: {
          color: '#FFFFFF',
          backgroundColor: '#DB8B3C',
        },
      },
      subHeader: {
        style: {
          alignItems: 'start',
          justifyContent: 'start',
        },
      },
    }

    //Dynamic data filtering, managed by DataTable lib
    const filteredItems = tableData.filter(item => {
        //If no filter criteria, add to filteredItems
        if(filterValues == null) return true;
        if(filterValues.name.value==='-1' && filterValues.email.value==='-1') {
          return true;
        }
        //Filter for each criteria
        if (filterValues.email.value!=='-1') {
          if(item.email.toLowerCase().trim() !== filterValues.email.label.toLowerCase().trim())
            return false;
        }
        return true;
    })
  
    return (
        <>
          <DataTable
              pagination
              columns={tableColumns}
              customStyles={customStyles}
              data={tableData}
              highlightOnHover={true}
              paginationComponentOptions={paginationOptions}
              paginationResetDefaultPage={resetPaginationToggle}
              noDataComponent={<div className="pb-4">{t('datatable.no-records-to-display')}</div>}
              progressPending={loading}
          />
        </>
    )
}

export default AdminUsersDataTable

/**
 * subHeaderComponent={<AdminUsersDatatableFilters 
                                    setData={setData}
                                    handleChange={handleFilterChange} 
                                    handleReset={handleClear} />}
 */