import { CImage } from '@coreui/react'
import impersonate_image from '../assets/images/contacted.png'
import userInfo_image from '../assets/images/account.png'

export function AdminUserActions ({ id, impersonate, userInfo }) {
    
    
    const handleImpersonate = () => {
        impersonate(id)
    }

    const handleUserInfo = () => {
        userInfo(id)
    }

    return (
        <>
            <CImage src={impersonate_image} className="isLink userActionLink" onClick={handleImpersonate} />
            <CImage src={userInfo_image} className="isLink userActionLink" onClick={handleUserInfo} />
        </>
    )
}