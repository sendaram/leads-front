import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import {
    CButton,
    CCol,
    CFormCheck,
    CFormSelect,
    CRow,
  } from '@coreui/react'
import { LeadsApi } from '../api/LeadsApi';
import { MarcasApi } from '../api/MarcasApi';
import { DateUtils } from '../utils/DateUtils';

const EMPTY_LABEL = ''
const DEFAULT_SELECT_VALUE = '-1'

export const initialFilterValues = {
    name : {label: EMPTY_LABEL, value: DEFAULT_SELECT_VALUE},
    email : {label: EMPTY_LABEL, value: DEFAULT_SELECT_VALUE}
}

export function AdminUsersDatatableFilters({setData, handleChange, handleReset }) {

    const { t } = useTranslation()
    const NAME_PLACEHOLDER_TXT = t('datatable.brand')
    const EMAIL_PLACEHOLDER_TXT = t('datatable.province')
    
    //Initialize filterValues in state
    const [filterValues, setFilterValues] = useState(initialFilterValues);

    //Initialize Model options in state, because depends on Brand selection
    const [ optionsName, setOptionsName ] = useState([{label: NAME_PLACEHOLDER_TXT, value: DEFAULT_SELECT_VALUE}]);
    const [ optionsEmail, setOptionsEmail ] = useState([{ label: EMAIL_PLACEHOLDER_TXT, value: DEFAULT_SELECT_VALUE }]);
    const [ modelFilterDisabled, setModelFilterDisabled ] = useState(true)
    const [apiCall, setAPiCall ] = useState(false);

    useEffect(() =>{
        let isMounted = true;
        (async () => {
            /*
            const apiFilters = await MarcasApi.getFilters({
                name: filterValues.name.value,
                email: filterValues.email.value
            });
            */
            // Marcas cargadas inicialmente, todas para las que se muestran leads
            const allNames = [];
            const allEmails = [];

            // Añadimos el campo inicial del desplegable
            allNames.unshift({label: NAME_PLACEHOLDER_TXT, value: DEFAULT_SELECT_VALUE})
            allEmails.unshift({label: EMAIL_PLACEHOLDER_TXT, value: DEFAULT_SELECT_VALUE})

            if(isMounted) {
                setOptionsName(allNames);
                setOptionsEmail(allEmails);
            }
        })()
        return () => { isMounted = false };
    }, [])
    
    useEffect(() => {
        return;
        let isMounted = true;
        (async () => {
            if(apiCall) return;
            if(!isMounted) return;
            setAPiCall(true);
            /*
            const apiFilters = await MarcasApi.getFilters({
                name: filterValues.name.value,
                email: filterValues.email.value
            });
            */

            const allNames = [];
            const allEmails = [];
            allNames.unshift({label: NAME_PLACEHOLDER_TXT, value: DEFAULT_SELECT_VALUE})
            allEmails.unshift({label: EMAIL_PLACEHOLDER_TXT, value: DEFAULT_SELECT_VALUE})
            //Update Model options on rendering component, depending on Brand value
            if( filterValues.name.value !== DEFAULT_SELECT_VALUE) {
                allNames.unshift({ label: NAME_PLACEHOLDER_TXT, value: DEFAULT_SELECT_VALUE });
                setOptionsName([...allNames])
                setModelFilterDisabled(false)

            } else if( filterValues.name.value === DEFAULT_SELECT_VALUE ) {
           
                setOptionsName([{ label: NAME_PLACEHOLDER_TXT, value: DEFAULT_SELECT_VALUE }]);
                setFilterValues(prevState => ({
                    ...prevState,
                    model: {
                        label: EMPTY_LABEL,
                        value: DEFAULT_SELECT_VALUE
                    },
                }))
                setModelFilterDisabled(true)
            }
            // setOptionsEmail([...segments]);
            // setOptionsProvince([...provincias]);
            // setOptionsBrand([...allBrands]);
            setAPiCall(false);
        })()
        handleChange(filterValues)
        return () => { isMounted = false };
    }, [filterValues])
    

    const optionsContacts = LeadsApi.getAllContacts()

    //UI methods
    const resetForm = () => {
        setFilterValues((prevState) => ({...prevState, ...initialFilterValues}))
        handleReset()
    }

    return (
        <CRow className="g-3 mb-4">
            <CCol xs="auto">
                <CFormSelect name="name" size="sm" className="filterField" value={filterValues.name.value} disabled={modelFilterDisabled}
                options={optionsName} onChange={e => setFilterValues({...filterValues, [e.target.name]: { label: e.target.options[e.target.selectedIndex].text, value : e.target.options[e.target.selectedIndex].value}})} />
            </CCol>
            <CCol xs="auto">
                <CFormSelect 
                name="email" size="sm" 
                className="filterField"
                value={filterValues.email.value}
                options={optionsEmail} onChange={e => setFilterValues({...filterValues, [e.target.name]: { label: e.target.options[e.target.selectedIndex].text, value : e.target.options[e.target.selectedIndex].value}})} />
            </CCol>
            
            <CCol xs="auto">
                <CButton type="reset" color="danger" onClick={resetForm} >
                    {t('panel.clear')}
                </CButton>
            </CCol>
        </CRow>
    )
}