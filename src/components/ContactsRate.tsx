import { CImage } from '@coreui/react'
import imgGreenContacts from '../assets/images/green-contacts-square.png'
import imgOrangeContacts from '../assets/images/orange-contacts-square.png'
import imgRedContacts from '../assets/images/red-contacts-square.png'
import imgNoContacts from '../assets/images/square.png'

export function ContactsRate ({ contacts }) {

    const getRateType = ( contacts ) => {
        switch(contacts) {
            case 1:
                return <>
                    <CImage src={imgGreenContacts} className="_pe-05" />
                    <CImage src={imgNoContacts} className="_pe-05" />
                    <CImage src={imgNoContacts} className="_pe-05" />
                    <CImage src={imgNoContacts} />
                </>;
            case 2:
                return <>
                    <CImage src={imgGreenContacts} className="_pe-05" />
                    <CImage src={imgGreenContacts} className="_pe-05" />
                    <CImage src={imgNoContacts} className="_pe-05" />
                    <CImage src={imgNoContacts} />
                </>;
            case 3:
                return <>
                    <CImage src={imgOrangeContacts} className="_pe-05" />
                    <CImage src={imgOrangeContacts} className="_pe-05" />
                    <CImage src={imgOrangeContacts} className="_pe-05" />
                    <CImage src={imgNoContacts} />
                </>;
            case 4:
                return <>
                    <CImage src={imgRedContacts} className="_pe-05" />
                    <CImage src={imgRedContacts} className="_pe-05" />
                    <CImage src={imgRedContacts} className="_pe-05" />
                    <CImage src={imgRedContacts} />
                </>;
            default:
                return <>
                    <CImage src={imgNoContacts} className="_pe-05" />
                    <CImage src={imgNoContacts} className="_pe-05" />
                    <CImage src={imgNoContacts} className="_pe-05" />
                    <CImage src={imgNoContacts} />
                </>;

        }
    }

    return (
        <>
            {getRateType(contacts)}
        </>
    )
}