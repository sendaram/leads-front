import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { CForm,
    CFormLabel,
    CFormInput,
    CFormSelect,
    CFormTextarea,
    CButton,
    CAlert
} from '@coreui/react'
import { LeadsApi } from '../api/LeadsApi'

const ModalAction = ({ lead, setShow, action}) => {

    const { t } = useTranslation()
    const [actionValues, setActionValues] = useState({action:"", message:""})
    const [actions, setActions] = useState([]);
    const [errorMsg, setErrorMsg] = useState(null)

    const dispatch = useDispatch();

    useEffect(() => {
        let isMounted = true;
        (async () => {
          try {
            // const actionsList = await LeadsApi.getActions();
            // actionsList.unshift({label: t('lead-card.select-action'), value: '-1'})
            // if (isMounted) setActions(actionsList)
          } catch (error) {
            console.log('Loading lead card error: ', error);
          }
        })()
        return () => { isMounted = false };
    }, [])

    useEffect(() => {
        // setErrorMsg(false)
    }, [actionValues])

    const handleSendAction = async (event) => {
        event.preventDefault();
        try {
            await LeadsApi.sendAction(
                lead.id,
                action,
                actionValues.message
            );
            setShow(false)
        } catch (error) {
            if(error.includes('type should not be empty')) {
                // setErrorMsg(t('lead-card.warning-select-action'));
            } else {
                dispatch({ type: 'set', errorMessage: error });
                dispatch({ type: 'set', showError: true });
                setShow(false)
            }
        }
    }

    return (
        <>
        {
            actions
            ?
            <CForm id="cardFormAction" onSubmit={handleSendAction}>
                <CFormInput type="text" id="formNameToAction" placeholder={lead.name+" "+lead.surname} readOnly plainText />
                <CFormLabel htmlFor="formNameToAction">{t('lead-card.' + action + ".description")}</CFormLabel> 
                
                {/*
                <CFormLabel htmlFor="formAction" className="mt-2">{t('lead-card.action')}</CFormLabel>
                
                <CFormSelect id="formAction" options={actions} name="action"
                    onChange={e => setActionValues({...actionValues, [e.target.name]: e.target.options[e.target.selectedIndex].value})} >
                </CFormSelect>
                */}
                <div></div>
                <CFormLabel htmlFor="formMessage" className="mt-2">{t('lead-card.message')}</CFormLabel>
                <CFormTextarea id="formMessage" name="message"
                    onChange={e => setActionValues({...actionValues, [e.target.name]: e.target.value})}></CFormTextarea>
                { errorMsg ?
                    <CAlert color='danger' className='pt-1 pb-1 mt-2'>{errorMsg}</CAlert>
                    : ''
                }
                <CFormLabel htmlFor="formMessage" className="mt-2">{t('lead-card.' + action + ".warning")}</CFormLabel>
                <div className="mt-3">
                    <CButton type="submit">{t('lead-card.save')}</CButton>
                    <CButton onClick={() => setShow(false)} color="secondary" className="ms-2">{t('lead-card.cancel')}</CButton>
                </div>
            </CForm>
            : ""
        }
        
        </>
       
    )
}

export default ModalAction