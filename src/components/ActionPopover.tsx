import React from 'react'
import { useTranslation } from 'react-i18next'
import { DateUtils } from '../utils/DateUtils'
import { 
    CImage,
    CPopover,
    CTableRow,
    CTableDataCell    
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilUser } from '@coreui/icons'
import separator from '../assets/images/separator.png'

const ActionPopover = ({ title, date, content, system }) => {

    const { t } = useTranslation();

    const dateFormatted = DateUtils.formatLocaleDate(date, t('common.date-locale'));

    return (
        <>
        <CPopover
            title={title}
            content={content}
            placement="top"
            offset={[175,0]}
            trigger="hover"
        >
            <CTableRow className={'isLink ' + (system ? 'leadHistorySystemRow' : 'leadHistoryRow')}>
                <CTableDataCell className="leadHistoryCell">
                    {dateFormatted} <CImage src={separator} className="leadHistoryIcon" /></CTableDataCell>
                <CTableDataCell className="ps-3 pe-4 text-uppercase leadHistoryCell">
                    {title} <CIcon className="leadHistoryIcon" icon={cilUser} size='sm' />
                </CTableDataCell>
            </CTableRow>
        </CPopover>
        </>
    )
}

export default ActionPopover